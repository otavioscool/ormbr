{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.driver.firedac;

interface

uses
  Classes,
  DB,
  Variants,
  FireDAC.Comp.Client,
  FireDAC.Phys.FBDef,
  FireDAC.Phys.IBBase,
  FireDAC.Phys.FB,
  FireDAC.Stan.Param,
  FireDAC.Comp.ScriptCommands,
  FireDAC.Stan.Util,
  FireDAC.Comp.Script,
  /// orm
  ormbr.driver.connection,
  ormbr.factory.interfaces,
  ormbr.types.database;

type
  /// <summary>
  /// Classe de conex�o concreta com dbExpress
  /// </summary>
  TDriverFireDAC = class(TDriverConnection)
  protected
    FConnection: TFDConnection;
    FSQLScript: TFDScript;
  public
    constructor Create(AConnection: TComponent; ADriverName: TDriverName); override;
    destructor Destroy; override;
    procedure Connect; override;
    procedure Disconnect; override;
    procedure ExecuteDirect(const ASQL: string); override;
    procedure ExecuteScript(const ASQL: string); override;
    function IsConnected: Boolean; override;
    function InTransaction: Boolean; override;
    function CreateQuery: IDBQuery; override;
    function CreateResultSet: IDBResultSet; override;
    function ExecuteSQL(const ASQL: string): IDBResultSet; override;
  end;

  TDriverQueryFireDAC = class(TDriverQuery)
  private
    FFDQuery: TFDQuery;
  protected
    procedure SetCommandText(ACommandText: string); override;
    function GetCommandText: string; override;
  public
    constructor Create(AConnection: TFDConnection);
    destructor Destroy; override;
    procedure ExecuteDirect; override;
    function ExecuteQuery: IDBResultSet; override;
  end;

  TDriverResultSetFireDAC = class(TDriverResultSet<TFDQuery>)
  public
    constructor Create(ADataSet: TFDQuery); override;
    destructor Destroy; override;
    function NotEof: Boolean; override;
    function RecordCount: Integer; override;
    function GetFieldValue(AFieldName: string): Variant; overload; override;
    function GetFieldValue(AFieldIndex: Integer): Variant; overload; override;
    function GetFieldType(AFieldName: string): TFieldType; overload; override;
  end;

implementation

{ TDriverFireDAC }

constructor TDriverFireDAC.Create(AConnection: TComponent; ADriverName: TDriverName);
begin
  inherited;
  FConnection := AConnection as TFDConnection;
  FDriverName := ADriverName;
  FSQLScript := TFDScript.Create(nil);
  try
    FSQLScript.Connection := FConnection;
    FSQLScript.SQLScripts.Add;
  except
    FSQLScript.Free;
    raise;
  end;
end;

destructor TDriverFireDAC.Destroy;
begin
  FConnection := nil;
  FSQLScript.Free;
  inherited;
end;

procedure TDriverFireDAC.Disconnect;
begin
  inherited;
  FConnection.Connected := False;
end;

procedure TDriverFireDAC.ExecuteDirect(const ASQL: string);
begin
  inherited;
  FConnection.ExecSQL(ASQL);
end;

procedure TDriverFireDAC.ExecuteScript(const ASQL: string);
begin
  inherited;
  FSQLScript.SQLScripts.Items[0].SQL.Text := ASQL;
  FSQLScript.ExecuteAll;
end;

function TDriverFireDAC.ExecuteSQL(const ASQL: string): IDBResultSet;
var
  oDBQuery: IDBQuery;
begin
  oDBQuery := TDriverQueryFireDAC.Create(FConnection);
  oDBQuery.CommandText := ASQL;
  Result := oDBQuery.ExecuteQuery;
end;

procedure TDriverFireDAC.Connect;
begin
  inherited;
  FConnection.Connected := True;
end;

function TDriverFireDAC.InTransaction: Boolean;
begin
  Result := FConnection.InTransaction;
end;

function TDriverFireDAC.IsConnected: Boolean;
begin
  inherited;
  Result := FConnection.Connected = True;
end;

function TDriverFireDAC.CreateQuery: IDBQuery;
begin
  Result := TDriverQueryFireDAC.Create(FConnection);
end;

function TDriverFireDAC.CreateResultSet: IDBResultSet;
var
  oDBQuery: IDBQuery;
begin
  oDBQuery := TDriverQueryFireDAC.Create(FConnection);
  Result   := oDBQuery.ExecuteQuery;
end;

{ TDriverDBExpressQuery }

constructor TDriverQueryFireDAC.Create(AConnection: TFDConnection);
begin
  if AConnection <> nil then
  begin
     FFDQuery := TFDQuery.Create(nil);
     try
       FFDQuery.Connection := AConnection;
     except
       FFDQuery.Free;
       raise;
     end;
  end;
end;

destructor TDriverQueryFireDAC.Destroy;
begin
  FFDQuery.Free;
  inherited;
end;

function TDriverQueryFireDAC.ExecuteQuery: IDBResultSet;
var
  oResultSet: TFDQuery;
  iFor: Integer;
begin
  oResultSet := TFDQuery.Create(nil);
  try
    oResultSet.Connection := FFDQuery.Connection;
    oResultSet.SQL.Text   := FFDQuery.SQL.Text;

    for iFor := 0 to FFDQuery.Params.Count - 1 do
    begin
      oResultSet.Params[iFor].DataType := FFDQuery.Params[iFor].DataType;
      oResultSet.Params[iFor].Value    := FFDQuery.Params[iFor].Value;
    end;
    oResultSet.Open;
  except
    oResultSet.Free;
    raise;
  end;
  Result := TDriverResultSetFireDAC.Create(oResultSet);
  if oResultSet.RecordCount = 0 then
     Result.FetchingAll := True;
end;

function TDriverQueryFireDAC.GetCommandText: string;
begin
  Result := FFDQuery.SQL.Text;
end;

procedure TDriverQueryFireDAC.SetCommandText(ACommandText: string);
begin
  inherited;
  FFDQuery.SQL.Text := ACommandText;
end;

procedure TDriverQueryFireDAC.ExecuteDirect;
begin
  FFDQuery.ExecSQL;
end;

{ TDriverResultSetFireDAC }

constructor TDriverResultSetFireDAC.Create(ADataSet: TFDQuery);
begin
  inherited;
  FDataSet:= ADataSet;
end;

destructor TDriverResultSetFireDAC.Destroy;
begin
  FDataSet.Free;
  inherited;
end;

function TDriverResultSetFireDAC.GetFieldValue(AFieldName: string): Variant;
var
  oField: TField;
begin
  oField := FDataSet.FieldByName(AFieldName);
  Result := GetFieldValue(oField.Index);
end;

function TDriverResultSetFireDAC.GetFieldType(AFieldName: string): TFieldType;
begin
  Result := FDataSet.FieldByName(AFieldName).DataType;
end;

function TDriverResultSetFireDAC.GetFieldValue(AFieldIndex: Integer): Variant;
begin
  if AFieldIndex > FDataSet.FieldCount -1  then
    Exit(Variants.Null);

  if FDataSet.Fields[AFieldIndex].IsNull then
     Result := Variants.Null
  else
     Result := FDataSet.Fields[AFieldIndex].Value;
end;

function TDriverResultSetFireDAC.NotEof: Boolean;
begin
  if not FFirstNext then
     FFirstNext := True
  else
     FDataSet.Next;

  Result := not FDataSet.Eof;
end;

function TDriverResultSetFireDAC.RecordCount: Integer;
begin
  Result := FDataSet.RecordCount;
end;

end.
