{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.factory.interfaces;

interface

uses
  DB,
  Classes,
  ormbr.types.database;

type
  /// <summary>
  /// Interface de conex�o
  /// </summary>
  IDBResultSet = interface
  ['{DF558CDD-6CB4-4B42-8D6B-E4FC5F2642EA}']
    function GetFetchingAll: Boolean;
    procedure SetFetchingAll(const Value: Boolean);
    function NotEof: Boolean;
    function RecordCount: Integer;
    function GetFieldValue(AFieldName: string): Variant; overload;
    function GetFieldValue(AFieldIndex: Integer): Variant; overload;
    function GetFieldType(AFieldName: string): TFieldType;
    property FetchingAll: Boolean read GetFetchingAll write SetFetchingAll;
  end;

  IDBQuery = interface
    ['{B3DCAD6E-2006-47BD-99DC-2C50EC808C4D}']
    procedure SetCommandText(ACommandText: string);
    function GetCommandText: string;
    procedure ExecuteDirect;
    function ExecuteQuery: IDBResultSet;
    property CommandText: string read GetCommandText write SetCommandText;
  end;

  IDBConnection = interface
    ['{3423CA41-501A-4267-91BC-FE95F6AE3B81}']
    procedure Connect;
    procedure Disconnect;
    procedure StartTransaction;
    procedure Commit;
    procedure Rollback;
    procedure ExecuteDirect(const ASQL: string);
    procedure ExecuteScript(const ASQL: string);
    function InTransaction: Boolean;
    function IsConnected: Boolean;
    function GetDriverName: TDriverName;
    function CreateQuery: IDBQuery;
    function CreateResultSet: IDBResultSet;
    function ExecuteSQL(const ASQL: string): IDBResultSet;
  end;

  IDBTransaction = interface
    ['{9D4BC4BB-EC5F-4494-98EE-10C3551FBD65}']
    procedure StartTransaction;
    procedure Commit;
    procedure Rollback;
  end;

implementation

end.
