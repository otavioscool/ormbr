{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.rtti.helper;

interface

uses
  Rtti,
  SysUtils,
  TypInfo,
  ormbr.mapping.rttiutils,
  ormbr.types.nullable,
  ormbr.mapping.attributes;

type
  TRttiTypeHelper = class helper for TRttiType
  public
    function GetAssociation: TArray<TCustomAttribute>;
  end;

  TRttiPropertyHelper = class helper for TRttiProperty
  public
    function  IsJoinColumn: Boolean;
    function  GetRestriction: TCustomAttribute;
    function  GetDictionary: TCustomAttribute;
    function  GetNullableValue(AInstance: Pointer): TValue;
    function  GetIndex: Integer;
    procedure SetNullableValue(AInstance: Pointer; ATypeInfo: PTypeInfo; AValue: Variant);
  end;

implementation

{ TRttiPropertyHelper }

function TRttiPropertyHelper.GetDictionary: TCustomAttribute;
var
  oAttribute: TCustomAttribute;
begin
   for oAttribute in Self.GetAttributes do
   begin
      if oAttribute is Dictionary then // Dictionary
         Exit(oAttribute);
   end;
   Exit(nil);
end;

function TRttiPropertyHelper.GetIndex: Integer;
begin
  Result := (Self as TRttiInstanceProperty).Index +1;
end;

function TRttiPropertyHelper.GetNullableValue(AInstance: Pointer): TValue;
var
  oValue: TValue;
  oValueField: TRttiField;
  oHasValueField: TRttiField;
begin
  if TRttiSingleton.GetInstance.IsNullable(Self.PropertyType.Handle) then
  begin
     oValue := Self.GetValue(AInstance);
     oHasValueField := Self.PropertyType.GetField('FHasValue');
     if Assigned(oHasValueField) then
     begin
       oValueField := Self.PropertyType.GetField('FValue');
       if Assigned(oValueField) then
          Result := oValueField.GetValue(oValue.GetReferenceToRawData);
     end
  end
  else
     Result := Self.GetValue(AInstance);
end;

function TRttiPropertyHelper.GetRestriction: TCustomAttribute;
var
  oAttribute: TCustomAttribute;
begin
   for oAttribute in Self.GetAttributes do
   begin
      if oAttribute is Restrictions then // Restrictions
         Exit(oAttribute);
   end;
   Exit(nil);
end;

function TRttiPropertyHelper.IsJoinColumn: Boolean;
var
  oAttribute: TCustomAttribute;
begin
   for oAttribute in Self.GetAttributes do
   begin
      if oAttribute is JoinColumn then // JoinColumn
         Exit(True);
   end;
   Exit(False);
end;

procedure TRttiPropertyHelper.SetNullableValue(AInstance: Pointer; ATypeInfo: PTypeInfo; AValue: Variant);
begin
   if ATypeInfo = TypeInfo(Nullable<Integer>) then
      Self.SetValue(AInstance, TValue.From(Nullable<Integer>.Create(AValue)))
   else
   if ATypeInfo = TypeInfo(Nullable<String>) then
      Self.SetValue(AInstance, TValue.From(Nullable<String>.Create(AValue)))
   else
   if ATypeInfo = TypeInfo(Nullable<TDateTime>) then
      Self.SetValue(AInstance, TValue.From(Nullable<TDateTime>.Create(AValue)))
   else
   if ATypeInfo = TypeInfo(Nullable<Currency>) then
      Self.SetValue(AInstance, TValue.From(Nullable<Currency>.Create(AValue)))
   else
   if ATypeInfo = TypeInfo(Nullable<Double>) then
      Self.SetValue(AInstance, TValue.From(Nullable<Double>.Create(AValue)))
   else
   if ATypeInfo = TypeInfo(Nullable<Boolean>) then
      Self.SetValue(AInstance, TValue.From(Nullable<Boolean>.Create(AValue)))
   else
   if ATypeInfo = TypeInfo(Nullable<TDate>) then
      Self.SetValue(AInstance, TValue.From(Nullable<TDate>.Create(AValue)))
end;

{ TRttiTypeHelper }

function TRttiTypeHelper.GetAssociation: TArray<TCustomAttribute>;
var
  oProperty: TRttiProperty;
  oAttrib: TCustomAttribute;
  iLength: Integer;
begin
   iLength := -1;
   for oProperty in Self.GetProperties do
   begin
      for oAttrib in oProperty.GetAttributes do
      begin
         if oAttrib is Association then // Association
         begin
           Inc(iLength);
           SetLength(Result, iLength+1);
           Result[iLength] := oAttrib;
         end;
      end;
   end;
end;

end.
