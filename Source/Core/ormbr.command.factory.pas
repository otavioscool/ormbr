{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.command.factory;

interface

uses
  DB,
  Rtti,
  Generics.Collections,
  ormbr.criteria,
  ormbr.factory.interfaces,
  ormbr.dml.generator,
  ormbr.command.selecter,
  ormbr.command.inserter,
  ormbr.command.deleter,
  ormbr.command.updater,
  ormbr.Types.database;

type
  TDMLCommandFactoryAbstract = class abstract
  end;

  TDMLCommandFactory = class(TDMLCommandFactoryAbstract)
  protected
    FConnection: IDBConnection;
    FDMLCommand: string;
    FCommandSelecter: TCommandSelecter;
    FCommandInserter: TCommandInserter;
    FCommandUpdater: TCommandUpdater;
    FCommandDeleter: TCommandDeleter;
  public
    constructor Create(const AObject: TObject; const AConnection: IDBConnection; const ADriverName: TDriverName); virtual;
    destructor Destroy; override;
    function GeneratorSelectAll(AClass: TClass; APageSize: Integer): IDBResultSet; virtual;
    function GeneratorSelectID(AClass: TClass; AID:TValue; APageSize: Integer): IDBResultSet; virtual;
    function GeneratorSelect(ASQL: ICriteria; APageSize: Integer): IDBResultSet; virtual;
    function GeneratorNextPacket: IDBResultSet; virtual;
    function GetDMLCommand: string;
    function ExistSequence: Boolean;
    procedure GeneratorUpdate(AObject: TObject); overload; virtual;
    procedure GeneratorUpdate(AObject: TObject; AModifiedFields: TDictionary<string, TField>); overload; virtual;
    procedure GeneratorInsert(AObject: TObject); virtual;
    procedure GeneratorDelete(AObject: TObject); virtual;
  end;

implementation

{ TDMLCommandFactory }

constructor TDMLCommandFactory.Create(const AObject: TObject; const AConnection: IDBConnection; const ADriverName: TDriverName);
begin
  FConnection := AConnection;
  FCommandSelecter := TCommandSelecter.Create(AConnection, ADriverName);
  FCommandInserter := TCommandInserter.Create(AConnection, ADriverName);
  FCommandUpdater  := TCommandUpdater.Create(AConnection, ADriverName);
  FCommandDeleter  := TCommandDeleter.Create(AConnection, ADriverName);
end;

destructor TDMLCommandFactory.Destroy;
begin
  FCommandSelecter.Free;
  FCommandDeleter.Free;
  FCommandInserter.Free;
  FCommandUpdater.Free;
  inherited;
end;

function TDMLCommandFactory.GetDMLCommand: string;
begin
  Result := FDMLCommand;
end;

function TDMLCommandFactory.ExistSequence: Boolean;
begin
  if FCommandInserter.Sequence <> nil then
    Result := FCommandInserter.Sequence.ExistSequence;
end;

procedure TDMLCommandFactory.GeneratorDelete(AObject: TObject);
begin
  FConnection.ExecuteDirect(FCommandDeleter.GenerateDelete(AObject));
  FDMLCommand := FCommandDeleter.GetDMLCommand;
end;

procedure TDMLCommandFactory.GeneratorInsert(AObject: TObject);
begin
  FConnection.ExecuteDirect(FCommandInserter.GenerateInsert(AObject));
  FDMLCommand := FCommandInserter.GetDMLCommand;
end;

function TDMLCommandFactory.GeneratorSelect(ASQL: ICriteria; APageSize: Integer): IDBResultSet;
begin
  FCommandSelecter.SetPageSize(APageSize);
  Result := FConnection.ExecuteSQL(FCommandSelecter.GenerateSelect(ASQL));
  FDMLCommand := FCommandSelecter.GetDMLCommand;
end;

function TDMLCommandFactory.GeneratorSelectAll(AClass: TClass; APageSize: Integer): IDBResultSet;
begin
  FCommandSelecter.SetPageSize(APageSize);
  Result := FConnection.ExecuteSQL(FCommandSelecter.GenerateSelectAll(AClass));
  FDMLCommand := FCommandSelecter.GetDMLCommand;
end;

function TDMLCommandFactory.GeneratorSelectID(AClass: TClass; AID: TValue; APageSize: Integer): IDBResultSet;
begin
  FCommandSelecter.SetPageSize(APageSize);
  Result := FConnection.ExecuteSQL(FCommandSelecter.GenerateSelectID(AClass, AID));
  FDMLCommand := FCommandSelecter.GetDMLCommand;
end;

function TDMLCommandFactory.GeneratorNextPacket: IDBResultSet;
begin
  Result := FConnection.ExecuteSQL(FCommandSelecter.GenerateNextPacket);
  FDMLCommand := FCommandSelecter.GetDMLCommand;
end;

procedure TDMLCommandFactory.GeneratorUpdate(AObject: TObject);
begin
  FConnection.ExecuteDirect(FCommandUpdater.GenerateUpdate(AObject));
  FDMLCommand := FCommandUpdater.GetDMLCommand;
end;

procedure TDMLCommandFactory.GeneratorUpdate(AObject: TObject; AModifiedFields: TDictionary<string, TField>);
begin
  FConnection.ExecuteDirect(FCommandUpdater.GenerateUpdate(AObject, AModifiedFields));
  FDMLCommand := FCommandUpdater.GetDMLCommand;
end;

end.
