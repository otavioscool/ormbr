{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.mapping.attributes;

interface

uses
  Classes,
  SysUtils,
  DB,
  Rtti,
  ormbr.mapping.exceptions,
  ormbr.types.mapping;

type
  Description = class(TCustomAttribute)
  protected
    FDescription: string;
  public
    constructor Create(ADescription: string = '');
    property Description: string read FDescription;
  end;

  // ColumnBase
  ColumnBase = class(Description)
  private
    FColumnName: String;
  protected
    constructor Create(const AColumnName: string; ADescription: string); overload; virtual;
  public
    property ColumnName: String read FColumnName write FColumnName;
  end;

  Entity = class(TCustomAttribute)
  private
    FName: String;
    FSchemaName: String;
  public
    constructor Create; overload;
    constructor Create(AName: string; ASchemaName: String); overload;
    property Name: String Read FName Write FName;
    property SchemaName: String Read FSchemaName Write FSchemaName;
  end;

  // Table
  Table = class(Description)
  private
    FName: String;
  public
    constructor Create; overload;
    constructor Create(AName: String); overload;
    constructor Create(AName, ADescription: String); overload;
    property Name: String Read FName Write FName;
  end;

  View = class(Description)
  private
    FName: String;
  public
    constructor Create; overload;
    constructor Create(AName: String); overload;
    constructor Create(AName, ADescription: String); overload;
    property Name: String Read FName Write FName;
  end;

  Trigger = class(Description)
  private
    FName: String;
    FTableName: String;
  public
    constructor Create; overload;
    constructor Create(AName, ATableName: String); overload;
    constructor Create(AName, ATableName, ADescription: String); overload;
    property TableName: String Read FTableName Write FTableName;
    property Name: String Read FName Write FName;
  end;

  Sequence = class(TCustomAttribute)
  private
    FName: string;
    FInitial: Integer;
    FIncrement: Integer;
  public
    constructor Create(AName: string; AInitial: Integer = 0; AIncrement: Integer = 1);
    property Name: string read FName write FName;
    property Initial: Integer read FInitial write FInitial;
    property Increment: Integer read FIncrement write FIncrement;
  end;

  // Column
  Column = class(ColumnBase)
  private
    FPosition: Integer;
    FFieldType: TFieldType;
    FScale: Integer;
    FSize: Integer;
    FPrecision: Integer;
  public
    constructor Create(AColumnName: String; AFieldType: TFieldType; ADescription: string = ''); overload;
    constructor Create(AColumnName: string; AFieldType: TFieldType; ASize: Integer; ADescription: string = ''); overload;
    constructor Create(AColumnName: string; AFieldType: TFieldType; APrecision, AScale: Integer; ADescription: string = ''); overload;
    property Position: Integer read FPosition write FPosition;
    property FieldType: TFieldType read FFieldType write FFieldType;
    property Size: Integer read FSize write FSize;
    property Scale: Integer read FScale write FScale;
    property Precision: Integer read FPrecision write FPrecision;
  end;

  /// Association master-details
  Association = class(TCustomAttribute)
  private
    FMultiplicity: TMultiplicity;
    FColumnName: string;
    FReferencedColumnName: string;
    FColumns: TArray<string>;
  public
    constructor Create(AMultiplicity: TMultiplicity; AColumnName, AReferencedColumnName: string); overload;
    constructor Create(AMultiplicity: TMultiplicity; AColumnName, AReferencedColumnName, AColumns: string); overload;
    property Multiplicity: TMultiplicity read FMultiplicity;
    property ColumnName: string read FColumnName;
    property ReferencedColumnName: string read FReferencedColumnName;
    property Columns: TArray<string> read FColumns;
  end;

  /// ForeignKey
  ForeignKey = class(Description)
  private
    FReferenceTableName: string;
    FRuleUpdate: TRuleAction;
    FRuleDelete: TRuleAction;
  public
    constructor Create(AReferenceTableName: string; ARuleDelete, ARuleUpdate: TRuleAction; ADescription: string = '');
    property ReferenceTableName: string read FReferenceTableName;
    property RuleDelete: TRuleAction read FRuleDelete;
    property RuleUpdate: TRuleAction read FRuleUpdate;
  end;

  /// PrimaryKey
  PrimaryKey = class(Description)
  private
    FColumns: TArray<string>;
    FSortingOrder: TSortingOrder;
    FUnique: Boolean;
    FSequenceType: TSequenceType;
  public
    constructor Create(AColumns, ADescription: string); overload;
    constructor Create(AColumns: string; ASequenceType: TSequenceType = NotInc; ASortingOrder: TSortingOrder = NoSort; AUnique: Boolean = False; ADescription: string = ''); overload;
    property Columns: TArray<string> read FColumns;
    property SortingOrder: TSortingOrder read FSortingOrder;
    property Unique: Boolean read FUnique;
    property SequenceType: TSequenceType read FSequenceType write FSequenceType;
  end;

  /// Indexe
  Indexe = class(Description)
  private
    FName: string;
    FColumns: TArray<string>;
    FSortingOrder: TSortingOrder;
    FUnique: Boolean;
  public
    constructor Create(AName, AColumns, ADescription: string); overload;
    constructor Create(AName, AColumns: string; ASortingOrder: TSortingOrder = NoSort;
      AUnique: Boolean = False; ADescription: string = ''); overload;
    property Name: string read FName;
    property Columns: TArray<string> read FColumns;
    property SortingOrder: TSortingOrder read FSortingOrder;
    property Unique: Boolean read FUnique;
  end;

  /// Indexe
  Check = class(Description)
  private
    FName: string;
    FCondition: string;
  public
    constructor Create(AName, ACondition: string; ADescription: string = '');
    property Name: string read FName;
    property Condition: string read FCondition;
  end;

  JoinColumn = class(TCustomAttribute)
  private
    FColumnName: string;
    FReferencedTableName: string;
    FReferencedColumnName: string;
    FJoin: TJoin;
  public
    constructor Create(AColumnName, AReferencedTableName, AReferencedColumnName: string; AJoin: TJoin = InnerJoin);
    property ColumnName: string read FColumnName;
    property ReferencedColumnName: string read FReferencedColumnName;
    property ReferencedTableName: string read FReferencedTableName;
    property Join: TJoin read FJoin write FJoin;
  end;

  Restrictions = class(TCustomAttribute)
  private
    FRestrictions: TRestrictions;
  public
    constructor Create(ARestrictions: TRestrictions);
    property Restrictions: TRestrictions read FRestrictions write FRestrictions;
  end;

  // Column Dictionary
  Dictionary = class(TCustomAttribute)
  private
    FDisplayLabel: string;
    FDefaultExpression: string;
    FConstraintErrorMessage: string;
    FDisplayFormat: string;
    FEditMask: string;
    FAlignment: TAlignment;
  public
    constructor Create(ADisplayLabel: string); overload;
    constructor Create(ADisplayLabel, AConstraintErrorMessage: string); overload;
    constructor Create(ADisplayLabel, AConstraintErrorMessage, ADefaultExpression: string); overload;
    constructor Create(ADisplayLabel, AConstraintErrorMessage, ADefaultExpression, ADisplayFormat: string); overload;
    constructor Create(ADisplayLabel, AConstraintErrorMessage, ADefaultExpression, ADisplayFormat, AEditMask: string); overload;
    constructor Create(ADisplayLabel, AConstraintErrorMessage, ADefaultExpression, ADisplayFormat, AEditMask: string;
      AAlignment: TAlignment); overload;
    constructor Create(ADisplayLabel, AConstraintErrorMessage: string; AAlignment: TAlignment); overload;
    property DisplayLabel: string read FDisplayLabel write FDisplayLabel;
    property ConstraintErrorMessage: string read FConstraintErrorMessage write FConstraintErrorMessage;
    property DefaultExpression: string read FDefaultExpression write FDefaultExpression;
    property DisplayFormat: string read FDisplayFormat write FDisplayFormat;
    property EditMask: string read FEditMask write FEditMask;
    property Alignment: TAlignment read FAlignment write FAlignment;
  end;

  NotNullConstraint = class(TCustomAttribute)
  public
    constructor Create;
    procedure Validate(AName: string; AValue : TValue);
  end;

  ZeroConstraint = class(TCustomAttribute)
  public
    constructor Create;
    procedure Validate(AName: string; AValue : TValue);
  end;

implementation

uses
  ormbr.mapping.rttiutils;

{ Table }

constructor Table.Create;
begin
  Create('');
end;

constructor Table.Create(AName: String);
begin
  Create(AName, '');
end;

constructor Table.Create(AName, ADescription: String);
begin
  inherited Create(ADescription);
  FName := AName;
end;

{ View }

constructor View.Create;
begin
  Create('');
end;

constructor View.Create(AName: String);
begin
  Create(AName, '');
end;

constructor View.Create(AName, ADescription: String);
begin
  inherited Create(ADescription);
  FName := AName;
end;

{ ColumnDictionary }

constructor Dictionary.Create(ADisplayLabel: string);
begin
   FAlignment := taLeftJustify;
   FDisplayLabel := ADisplayLabel;
end;

constructor Dictionary.Create(ADisplayLabel, AConstraintErrorMessage: string);
begin
   Create(ADisplayLabel);
   FConstraintErrorMessage := AConstraintErrorMessage;
end;

constructor Dictionary.Create(ADisplayLabel, AConstraintErrorMessage, ADefaultExpression: string);
begin
   Create(ADisplayLabel, AConstraintErrorMessage);
   FDefaultExpression := ADefaultExpression;
end;

constructor Dictionary.Create(ADisplayLabel, AConstraintErrorMessage, ADefaultExpression, ADisplayFormat: string);
begin
   Create(ADisplayLabel, AConstraintErrorMessage, ADefaultExpression);
   FDisplayFormat := ADisplayFormat;
end;

constructor Dictionary.Create(ADisplayLabel,
  AConstraintErrorMessage, ADefaultExpression, ADisplayFormat, AEditMask: string;
  AAlignment: TAlignment);
begin
   Create(ADisplayLabel, AConstraintErrorMessage, ADefaultExpression, ADisplayFormat, AEditMask);
   FAlignment := AAlignment;
end;

constructor Dictionary.Create(ADisplayLabel, AConstraintErrorMessage,
  ADefaultExpression, ADisplayFormat, AEditMask: string);
begin
  Create(ADisplayLabel, AConstraintErrorMessage, ADefaultExpression, ADisplayFormat);
  FEditMask := AEditMask;
end;

constructor Dictionary.Create(ADisplayLabel, AConstraintErrorMessage: string;
  AAlignment: TAlignment);
begin
   Create(ADisplayLabel, AConstraintErrorMessage);
   FAlignment := AAlignment;
end;

{ Column }

constructor Column.Create(AColumnName: String; AFieldType: TFieldType; ADescription: string);
begin
   Create(AColumnName, AFieldType, 0, ADescription);
end;

constructor Column.Create(AColumnName: string; AFieldType: TFieldType; ASize: Integer; ADescription: string);
begin
   Create(AColumnName, AFieldType, 0, 0, ADescription);
   FSize := ASize;
end;

constructor Column.Create(AColumnName: string; AFieldType: TFieldType; APrecision, AScale: Integer; ADescription: string);
begin
   FColumnName := AColumnName;
   FFieldType := AFieldType;
   FPrecision := APrecision;
   FScale := AScale;
   FDescription := ADescription;
end;

{ ColumnRestriction }

constructor Restrictions.Create(ARestrictions: TRestrictions);
begin
  FRestrictions := ARestrictions;
end;

{ NotNull }

constructor NotNullConstraint.Create;
begin

end;

procedure NotNullConstraint.Validate(AName: string; AValue: TValue);
begin
  if AValue.AsString = '' then
  begin
     raise EFieldNotNull.Create(AName);
  end;
end;

{ Association }

constructor Association.Create(AMultiplicity: TMultiplicity; AColumnName, AReferencedColumnName: string);
begin
  FMultiplicity := AMultiplicity;
  FColumnName := AColumnName;
  FReferencedColumnName := AReferencedColumnName;
end;

constructor Association.Create(AMultiplicity: TMultiplicity; AColumnName, AReferencedColumnName, AColumns: string);
var
  rColumns: TStrArray;
  iFor: Integer;
begin
  Create(AMultiplicity, AColumnName, AreferencedColumnName);
  if Length(AColumns) > 0 then
  begin
    SetLength(FColumns, TRttiSingleton.GetInstance.Explode(rColumns,';',AColumns));
    for iFor := Low(rColumns) to High(rColumns) do
      FColumns[iFor] := rColumns[iFor];
  end;
end;

{ ColumnBase }

constructor ColumnBase.Create(const AColumnName: string; ADescription: string);
begin
  inherited Create(ADescription);
  FColumnName := AColumnName;
end;

{ JoinColumn }

constructor JoinColumn.Create(AColumnName, AReferencedTableName, AReferencedColumnName: string; AJoin: TJoin);
begin
  FColumnName := AColumnName;
  FReferencedTableName := AReferencedTableName;
  FReferencedColumnName := AReferencedColumnName;
  FJoin := AJoin;
end;

{ ForeignKey }

constructor ForeignKey.Create(AReferenceTableName: string; ARuleDelete,
  ARuleUpdate: TRuleAction; ADescription: string);
begin
  inherited Create(ADescription);
  FReferenceTableName := AReferenceTableName;
  FRuleDelete := ARuleUpdate;
  FRuleUpdate := ARuleUpdate;
end;

{ Description }

constructor Description.Create(ADescription: string);
begin
  FDescription := ADescription;
end;

{ PrimaryKey }

constructor PrimaryKey.Create(AColumns, ADescription: string);
begin
  Create(AColumns, NotInc, NoSort, False, ADescription);
end;

constructor PrimaryKey.Create(AColumns: string; ASequenceType: TSequenceType; ASortingOrder: TSortingOrder; AUnique: Boolean; ADescription: string);
var
  rColumns: TStrArray;
  iFor: Integer;
begin
  inherited Create(ADescription);
  if Length(AColumns) > 0 then
  begin
    SetLength(FColumns, TRttiSingleton.GetInstance.Explode(rColumns,';',AColumns));
    for iFor := Low(rColumns) to High(rColumns) do
      FColumns[iFor] := rColumns[iFor];
  end;
  FSequenceType := ASequenceType;
  FSortingOrder := ASortingOrder;
  FUnique := AUnique;
end;

{ Catalog }

constructor Entity.Create(AName: string; ASchemaName: String);
begin
  FName := AName;
  FSchemaName := ASchemaName;
end;

constructor Entity.Create;
begin
  Create('','');
end;

{ ZeroConstraint }

constructor ZeroConstraint.Create;
begin

end;

procedure ZeroConstraint.Validate(AName: string; AValue: TValue);
begin
  if AValue.AsInteger < 0 then
  begin
     raise EFieldZero.Create(AName);
  end;
end;

{ Sequence }

constructor Sequence.Create(AName: string; AInitial, AIncrement: Integer);
begin
  FName := AName;
  FInitial := AInitial;
  FIncrement := AIncrement;
end;

{ Trigger }

constructor Trigger.Create;
begin
  Create('','');
end;

constructor Trigger.Create(AName, ATableName: String);
begin
  Create(AName, ATableName, '')
end;

constructor Trigger.Create(AName, ATableName, ADescription: String);
begin
  inherited Create(ADescription);
  FName := AName;
  FTableName := ATableName;
end;

{ Indexe }

constructor Indexe.Create(AName, AColumns, ADescription: string);
begin
  Create(AName, AColumns, NoSort, False, ADescription);
end;

constructor Indexe.Create(AName, AColumns: string; ASortingOrder: TSortingOrder;
  AUnique: Boolean; ADescription: string);
var
  rColumns: TStrArray;
  iFor: Integer;
begin
  FName := AName;
  if Length(AColumns) > 0 then
  begin
    SetLength(FColumns, TRttiSingleton.GetInstance.Explode(rColumns,';',AColumns));
    for iFor := Low(rColumns) to High(rColumns) do
      FColumns[iFor] := rColumns[iFor];
  end;
  FSortingOrder := ASortingOrder;
  FUnique := AUnique;
  FDescription := ADescription;
end;

{ Check }

constructor Check.Create(AName, ACondition, ADescription: string);
begin
  inherited Create(ADescription);
  FName := AName;
  FCondition := ACondition;
end;

end.
