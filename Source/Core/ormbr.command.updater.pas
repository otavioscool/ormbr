{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.command.updater;

interface

uses
  DB,
  Generics.Collections,
  ormbr.command.abstract,
  ormbr.factory.interfaces,
  ormbr.types.database;

type
  TCommandUpdater = class(TDMLCommandAbstract)
  private
  public
    constructor Create(AConnection: IDBConnection; ADriverName: TDriverName); override;
    function GenerateUpdate(AObject: TObject): string; overload;
    function GenerateUpdate(AObject: TObject; AModifiedFields: TDictionary<string, TField>): string; overload;
  end;

implementation

{ TCommandUpdater }

function TCommandUpdater.GenerateUpdate(AObject: TObject): string;
begin
  FDMLCommand := FDMLGeneratorCommand.GeneratorUpdate(AObject);
  Result := FDMLCommand;
end;

constructor TCommandUpdater.Create(AConnection: IDBConnection; ADriverName: TDriverName);
begin
  inherited Create(AConnection, ADriverName);
end;

function TCommandUpdater.GenerateUpdate(AObject: TObject; AModifiedFields: TDictionary<string, TField>): string;
begin
  FDMLCommand := FDMLGeneratorCommand.GeneratorUpdate(AObject, AModifiedFields);
  Result := FDMLCommand;
end;

end.
