{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.mapping.classes;

interface

uses
  DB,
  TypInfo,
  Rtti,
  SysUtils,
  Generics.Collections,
  ormbr.mapping.attributes,
  ormbr.types.mapping;

type
  TMappingDescription = class abstract
  protected
    FDescription: string;
  public
    property Description: string read FDescription write FDescription;
  end;

  TColumnBase = class(TMappingDescription)
  private
    FName: string;
    FFieldIndex: Integer;
    FFieldType: TFieldType;
    FDefaultValue: string;
    FNotNull: Boolean;
    FCheck: Boolean;
    FUnique: Boolean;
  public
    constructor Create(AFieldIndex: Integer; AName: string); overload;
    constructor Create(AFieldIndex: Integer; AName: string; AFieldType: TFieldType); overload;
    property Name: string read FName write FName;
    property FieldIndex: Integer read FFieldIndex write FFieldIndex;
    property FieldType: TFieldType read FFieldType write FFieldType;
    property DefaultValue: string read FDefaultValue write FDefaultValue;
    property NotNull: Boolean read FNotNull write FNotNull;
    property Check: Boolean read FCheck write FCheck;
    property Unique: Boolean read FUnique write FUnique;
  end;

  /// TableMapping
  TTableMapping = class(TMappingDescription)
  private
    FName: string;
    FSchema: string;
  public
    property Name: string read FName write FName;
    property Schema: string read FSchema write FSchema;
  end;

  TSequenceMapping = class(TMappingDescription)
  private
    FTableName: string;
    FName: string;
    FInitial: Integer;
    FIncrement: Integer;
  public
    property TableName: string read FTableName write FTableName;
    property Name: string read FName write FName;
    property Initial: Integer read FInitial write FInitial;
    property Increment: Integer read FIncrement write FIncrement;
  end;

  /// TriggerMapping
  TTriggerMapping = class(TMappingDescription)
  private
    FName: string;
    FScript: string;
  public
    constructor Create(AName, AScript: string);
    property Name: string read FName write FName;
    property Script: string read FScript write FScript;
  end;
  /// ColumnMappingList
  TTriggerMappingList = class(TObjectList<TTriggerMapping>);

  /// ColumnMapping
  TColumnMapping = class(TColumnBase)
  private
    FSize: Integer;
    FPrecision: Integer;
    FScale: Integer;
  public
    property Size: Integer read FSize write FSize;
    property Precision: Integer read FPrecision write FPrecision;
    property Scale: Integer read FScale write FScale;
  end;
  /// ColumnMappingList
  TColumnMappingList = class(TObjectList<TColumnMapping>);

  /// AssociationMapping
  TAssociationMapping = class
  private
    FMultiplicity: TMultiplicity;
    FColumnName: string;
    FReferencedColumnName: string;
    FReferencedClassName: string;
    FColumns: TArray<string>;
    function GetColumns(Index: Integer): string;
    function GetCount: Integer;
  public
    constructor Create(AMultiplicity: TMultiplicity; AColumnName, AReferencedColumnName,
      AReferencedClassName: string); overload;
    constructor Create(AMultiplicity: TMultiplicity; AColumnName, AReferencedColumnName,
      AReferencedClassName: string;
      AColumns: TArray<string>); overload;
    property Multiplicity: TMultiplicity read FMultiplicity;
    property ColumnName: string read FColumnName;
    property ReferencedColumnName: string read FReferencedColumnName;
    property ReferencedClassName: string read FReferencedClassName;
    property Columns[index: Integer]: string read GetColumns;
    property Count: Integer read GetCount;
  end;
  /// AssociationMappingList
  TAssociationMappingList = class(TObjectList<TAssociationMapping>);

  /// IndexeMapping
  TPrimaryKeyMapping = class(TMappingDescription)
  private
    FName: string;
    FColumns: TArray<string>;
    FSortingOrder: TSortingOrder;
    FUnique: Boolean;
    function GetColumns(Index: Integer): string;
    function GetColumnsCount: Integer;
  public
    constructor Create(AColumns: TArray<string>; ASortingOrder: TSortingOrder;
      AUnique: Boolean; ADescription: string = '');
    property Name: string read FName write FName;
    property Columns[index: Integer]: string read GetColumns;
    property SortingOrder: TSortingOrder read FSortingOrder write FSortingOrder;
    property Unique: Boolean read FUnique write FUnique;
    property ColumnsCount: Integer read GetColumnsCount;
  end;

  /// PrimaryKeyMapping
  TIndexeMapping = class(TPrimaryKeyMapping)
  public
    constructor Create(AName: string; AColumns: TArray<string>; ASortingOrder: TSortingOrder;
      AUnique: Boolean; ADescription: string = '');
  end;

  /// IndexeMappingList
  TIndexeMappingList = class(TObjectList<TIndexeMapping>);

  /// CheckMapping
  TCheckMapping = class(TMappingDescription)
  private
    FName: string;
    FCondition: string;
  public
    constructor Create(AName, ACondition, ADescription: string);
    property Name: string read FName write FName;
    property Condition: string read FCondition write FCondition;
  end;

  /// CheckMappingList
  TCheckMappingList = class(TObjectList<TCheckMapping>);

  /// ForeignKeyKeyMapping
  TForeignKeyMapping = class(TMappingDescription)
  private
    FName: string;
    FReferenceTableName: string;
    FFromColumns: string;
    FToColumns: string;
    FRuleUpdate: TRuleAction;
    FRuleDelete: TRuleAction;
  public
    constructor Create(AReferenceTableName, AFromColumns, AToColumns: string;
      ARuleDelete, ARuleUpdate: TRuleAction; ADescription: string = '');
    property Name: string read FName write FName;
    property ReferenceTableName: string read FReferenceTableName;
    property FromColumns: string read FFromColumns write FFromColumns;
    property ToColumns: string read FToColumns write FToColumns;
    property RuleDelete: TRuleAction read FRuleDelete;
    property RuleUpdate: TRuleAction read FRuleUpdate;
  end;
  /// ForeignKeyMappingList
  TForeignKeyMappingList = class(TObjectList<TForeignKeyMapping>);

  /// JoinColumnMapping
  TJoinColumnMapping = class
  private
    FColumnName: string;
    FReferencedTableName: string;
    FReferencedColumnName: string;
    FJoin: TJoin;
  public
    constructor Create(AColumnName, AReferencedTableName, AReferencedColumnName: string;
      AJoin: TJoin = InnerJoin);
    property ColumnName: string read FColumnName;
    property ReferencedColumnName: string read FReferencedColumnName;
    property ReferencedTableName: string read FReferencedTableName;
    property Join: TJoin read FJoin write FJoin;
  end;
  /// JoinColumnMappingList
  TJoinColumnMappingList = class(TObjectList<TJoinColumnMapping>);

  /// ViewMapping
  TViewMapping = class(TMappingDescription)
  private
    FName: string;
    FScript: string;
  public
    property Name: string read FName write FName;
    property Script: string read FScript write FScript;
  end;

implementation

{ TIdColumnMapping }

constructor TColumnBase.Create(AFieldIndex: Integer; AName: string);
begin
  FFieldIndex := AFieldIndex;
  FName := AName;
end;

constructor TColumnBase.Create(AFieldIndex: Integer; AName: string; AFieldType: TFieldType);
begin
  Create(AFieldIndex, AName);
  FFieldType := AFieldType;
end;

{ TOneToOneRelationMapping }

constructor TAssociationMapping.Create(AMultiplicity: TMultiplicity; AColumnName,
  AReferencedColumnName, AReferencedClassName: string);
begin
  FMultiplicity := AMultiplicity;
  FColumnName := AColumnName;
  FReferencedColumnName := AReferencedColumnName;
  FReferencedClassName := AReferencedClassName;
end;

constructor TAssociationMapping.Create(AMultiplicity: TMultiplicity; AColumnName,
  AReferencedColumnName, AReferencedClassName: string; AColumns: TArray<string>);
var
  iFor: Integer;
begin
  Create(AMultiplicity, AColumnName, AReferencedColumnName, AReferencedClassName);
  if Length(AColumns) > 0 then
  begin
    SetLength(FColumns, Length(AColumns));
    for iFor := Low(AColumns) to High(AColumns) do
      FColumns[iFor] := AColumns[iFor];
  end;
end;

function TAssociationMapping.GetColumns(index: Integer): string;
begin
  Result := FColumns[index];
end;

function TAssociationMapping.GetCount: Integer;
begin
  Result := Length(FColumns);
end;

{ TPrimaryKeyMapping }

constructor TPrimaryKeyMapping.Create(AColumns: TArray<string>;
  ASortingOrder: TSortingOrder; AUnique: Boolean; ADescription: string);
var
  iFor: Integer;
begin
  FSortingOrder := ASortingOrder;
  FUnique := AUnique;
  FDescription := ADescription;
  if Length(AColumns) > 0 then
  begin
    SetLength(FColumns, Length(AColumns));
    for iFor := Low(AColumns) to High(AColumns) do
      FColumns[iFor] := Trim(AColumns[iFor]);
  end;
end;

function TPrimaryKeyMapping.GetColumns(Index: Integer): string;
begin
  Result := FColumns[index];
end;

function TPrimaryKeyMapping.GetColumnsCount: Integer;
begin
  Result := Length(FColumns);
end;

{ TIndexMapping }

constructor TIndexeMapping.Create(AName: string; AColumns: TArray<string>;
  ASortingOrder: TSortingOrder; AUnique: Boolean; ADescription: string);
var
  iFor: Integer;
begin
  FName := AName;
  FSortingOrder := ASortingOrder;
  FUnique := AUnique;
  FDescription := ADescription;
  if Length(AColumns) > 0 then
  begin
    SetLength(FColumns, Length(AColumns));
    for iFor := Low(AColumns) to High(AColumns) do
      FColumns[iFor] := AColumns[iFor];
  end;
end;

{ TJoinColumnMapping }

constructor TJoinColumnMapping.Create(AColumnName, AReferencedTableName, AReferencedColumnName: string; AJoin: TJoin);
begin
  FColumnName := AColumnName;
  FReferencedTableName := AReferencedTableName;
  FReferencedColumnName := AReferencedColumnName;
  FJoin := AJoin;
end;

{ TForeignKeyMapping }

constructor TForeignKeyMapping.Create(AReferenceTableName, AFromColumns, AToColumns: string;
  ARuleDelete, ARuleUpdate: TRuleAction; ADescription: string);
begin
  FName := Format('FK_%s_%s', [AReferenceTableName, AFromColumns]);
  FReferenceTableName := AReferenceTableName;
  FFromColumns := AFromColumns;
  FToColumns := AToColumns;
  FRuleDelete := ARuleDelete;
  FRuleUpdate := ARuleUpdate;
  FDescription := ADescription;
end;

{ TTriggerMapping }

constructor TTriggerMapping.Create(AName, AScript: string);
begin
  FName := AName;
  FScript := AScript;
end;

{ TCheckMapping }

constructor TCheckMapping.Create(AName, ACondition, ADescription: string);
begin
  FName := AName;
  FCondition := ACondition;
  FDescription := ADescription;
end;

end.
