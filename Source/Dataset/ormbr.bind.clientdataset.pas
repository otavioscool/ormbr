{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.bind.clientdataset;

interface

uses
  Classes,
  SysUtils,
  DB,
  Rtti,
  DBClient,
  Variants,
  Generics.Collections,
  /// orm
  ormbr.criteria,
  ormbr.bind.base,
  ormbr.bind.events,
  ormbr.objects.manager,
  ormbr.factory.interfaces,
  ormbr.types.mapping,
  ormbr.objects.helper,
  ormbr.rtti.helper,
  ormbr.mapping.exceptions,
  ormbr.mapping.attributes;

type
  /// <summary>
  /// Captura de eventos espec�ficos do componente TClientDataSet
  /// </summary>
  TClientDataSetEvents = class(TDataSetEvents)
  private
    FBeforeApplyUpdates: TRemoteEvent;
    FAfterApplyUpdates: TRemoteEvent;
  public
    property BeforeApplyUpdates: TRemoteEvent read FBeforeApplyUpdates write FBeforeApplyUpdates;
    property AfterApplyUpdates: TRemoteEvent read FAfterApplyUpdates write FAfterApplyUpdates;
  end;

  /// <summary>
  /// Adapter TClientDataSet para controlar o Modelo e o Controle definido por:
  /// M - Object Model
  /// </summary>
  TClientDataSetAdapter<M: class, constructor> = class(TDatasetBase<M>)
  private
    FOrmDataSet: TClientDataSet;
    FClientDataSetEvents: TClientDataSetEvents;
    procedure DoBeforeApplyUpdates(Sender: TObject; var OwnerData: OleVariant);
    procedure DoAfterApplyUpdates(Sender: TObject; var OwnerData: OleVariant);
    procedure ExecuteCheckNotNull;
  protected
    procedure DoBeforePost(DataSet: TDataSet); override;
    procedure EmptyDataSetChilds; override;
    procedure GetDataSetEvents; override;
    procedure SetDataSetEvents; override;
    procedure ApplyInserter(MaxErros: Integer); override;
    procedure ApplyUpdater(MaxErros: Integer); override;
    procedure ApplyDeleter(MaxErros: Integer); override;
    procedure ApplyInternal(MaxErros: Integer); override;
  public
    constructor Create(AConnection: IDBConnection; ADataSet: TDataSet; APageSize: Integer; AMasterObject: TObject); override;
    destructor Destroy; override;
    /// Comandos DataSet
    procedure Open(ASQL: ICriteria; AID: TValue); override;
    procedure ApplyUpdates(MaxErros: Integer); override;
  end;

implementation

uses
  ormbr.bind.dataset,
  ormbr.bind.fields;

{ TClientDataSetAdapter<M> }

constructor TClientDataSetAdapter<M>.Create(AConnection: IDBConnection;
  ADataSet: TDataSet; APageSize: Integer; AMasterObject: TObject);
begin
  inherited Create(AConnection, ADataSet, APageSize, AMasterObject);
  /// Captura o component TClientDataset da IDE passado como par�metro
  FOrmDataSet := ADataSet as TClientDataSet;
  FClientDataSetEvents := TClientDataSetEvents.Create;
  /// Captura e guarda os eventos do dataset
  GetDataSetEvents;
  /// Seta os eventos do ORM no dataset, para que ele sejam disparados
  SetDataSetEvents;
end;

destructor TClientDataSetAdapter<M>.Destroy;
begin
  FOrmDataSet := nil;
  FClientDataSetEvents.Free;
  inherited;
end;

procedure TClientDataSetAdapter<M>.DoAfterApplyUpdates(Sender: TObject;
  var OwnerData: OleVariant);
begin
  if Assigned(FClientDataSetEvents.AfterApplyUpdates) then
    FClientDataSetEvents.AfterApplyUpdates(Sender, OwnerData);
end;

procedure TClientDataSetAdapter<M>.DoBeforeApplyUpdates(Sender: TObject;
  var OwnerData: OleVariant);
begin
  if Assigned(FClientDataSetEvents.BeforeApplyUpdates) then
    FClientDataSetEvents.BeforeApplyUpdates(Sender, OwnerData);
end;

procedure TClientDataSetAdapter<M>.DoBeforePost(DataSet: TDataSet);
begin
  inherited DoBeforePost(DataSet);
  ExecuteCheckNotNull;
end;

procedure TClientDataSetAdapter<M>.EmptyDataSetChilds;
var
  oChild: TPair<string, TDatasetBase<M>>;
begin
  inherited;
  for oChild in FMasterObject do
  begin
     if TClientDataSetAdapter<M>(oChild.Value).FOrmDataSet.Active then
       TClientDataSetAdapter<M>(oChild.Value).FOrmDataSet.EmptyDataSet;
  end;
end;

procedure TClientDataSetAdapter<M>.GetDataSetEvents;
begin
  inherited;
  if Assigned(FOrmDataSet.BeforeApplyUpdates) then FClientDataSetEvents.BeforeApplyUpdates := FOrmDataSet.BeforeApplyUpdates;
  if Assigned(FOrmDataSet.AfterApplyUpdates)  then FClientDataSetEvents.AfterApplyUpdates  := FOrmDataSet.AfterApplyUpdates;
end;

procedure TClientDataSetAdapter<M>.ApplyInternal(MaxErros: Integer);
var
  oRecnoBook: TBookmark;
  oProperty: TRttiProperty;
begin
  oRecnoBook := FOrmDataSet.Bookmark;
  FOrmDataSet.DisableControls;
//  FOrmDataSet.DisableConstraints;
  DisableDataSetEvents;
  try
    ApplyInserter(MaxErros);
    ApplyUpdater(MaxErros);
    ApplyDeleter(MaxErros);
    if FOrmDataSet.ChangeCount > 0 then
       FOrmDataSet.MergeChangeLog;
  finally
    FOrmDataSet.GotoBookmark(oRecnoBook);
    FOrmDataSet.FreeBookmark(oRecnoBook);
//    FOrmDataSet.EnableConstraints;
    FOrmDataSet.EnableControls;
    EnableDataSetEvents;
  end;
end;

procedure TClientDataSetAdapter<M>.ApplyDeleter(MaxErros: Integer);
begin
  inherited;
  /// Filtar somente os registros exclu�dos
  FOrmDataSet.StatusFilter := [usDeleted];
  try
    if FOrmDataSet.RecordCount > 0 then
    begin
       FOrmDataSet.First;
       while not FOrmDataSet.Eof do
       begin
         FSession.Manager.DeleteCommand(CurrentRecordInternal);
         FOrmDataSet.Edit;
         FOrmDataSet.Fields[FInternalIndex].AsInteger := -1;
         FOrmDataSet.Post;
         FOrmDataSet.Next;
       end;
    end;
  finally
    FOrmDataSet.StatusFilter := [];
  end;
end;

procedure TClientDataSetAdapter<M>.ApplyInserter(MaxErros: Integer);
var
  oProperty: TRttiProperty;
begin
  inherited;
  /// Filtar somente os registros inseridos
  FOrmDataSet.Filter := cInternalField + '=' + IntToStr(Integer(dsInsert));
  FOrmDataSet.Filtered := True;
  FOrmDataSet.First;
  try
    while not FOrmDataSet.Eof do
    begin
       /// Append/Insert
       if TDataSetState(FOrmDataSet.Fields[FInternalIndex].AsInteger) in [dsInsert] then
       begin
         FSession.Manager.InsertCommand(CurrentRecordInternal);
         FOrmDataSet.Edit;
         if FSession.Manager.ExistSequence then
           for oProperty in FCurrentRecordInternal.GetPrimaryKey do
             FOrmDataSet.Fields[oProperty.GetIndex].Value := oProperty.GetValue(TObject(FCurrentRecordInternal)).AsVariant;
         FOrmDataSet.Fields[FInternalIndex].AsInteger := -1;
         FOrmDataSet.Post;
       end;
       FOrmDataSet.Next;
    end;
  finally
    FOrmDataSet.Filtered := False;
    FOrmDataSet.Filter := '';
  end;
end;

procedure TClientDataSetAdapter<M>.ApplyUpdater(MaxErros: Integer);
var
  oProperty: TRttiProperty;
begin
  inherited;
  /// Filtar somente os registros modificados
  FOrmDataSet.Filter := cInternalField + '=' + IntToStr(Integer(dsEdit));
  FOrmDataSet.Filtered := True;
  FOrmDataSet.First;
  try
    while not FOrmDataSet.Eof do
    begin
       /// Edit
       if TDataSetState(FOrmDataSet.Fields[FInternalIndex].AsInteger) in [dsEdit] then
       begin
         FSession.Manager.UpdateCommand(TObject(CurrentRecordInternal), FModifiedFields);
         FOrmDataSet.Edit;
         FOrmDataSet.Fields[FInternalIndex].AsInteger := -1;
         FOrmDataSet.Post;
       end;
       FOrmDataSet.Next;
    end;
  finally
    FOrmDataSet.Filtered := False;
    FOrmDataSet.Filter := '';
  end;
end;

procedure TClientDataSetAdapter<M>.ApplyUpdates(MaxErros: Integer);
var
  oDetail: TPair<string, TDatasetBase<M>>;
  oWonerData: OleVariant;
begin
  inherited;
  if not FConnection.IsConnected then
     FConnection.Connect;
  try
    FConnection.StartTransaction;
    /// Before Apply
    DoBeforeApplyUpdates(FOrmDataSet, oWonerData);
    try
      ApplyInternal(MaxErros);
      /// Apply Details
      for oDetail in FMasterObject do
          oDetail.Value.ApplyInternal(MaxErros);
      /// After Apply
      DoAfterApplyUpdates(FOrmDataSet, oWonerData);
      FConnection.Commit;
    except
      FConnection.Rollback;
    end;
  finally
    FModifiedFields.Clear;
    if FConnection.IsConnected then
       FConnection.Disconnect;
  end;
end;

procedure TClientDataSetAdapter<M>.Open(ASQL: ICriteria; AID: TValue);
begin
  FOrmDataSet.DisableControls;
//  FOrmDataSet.DisableConstraints;
  DisableDataSetEvents;
  FConnection.Connect;
  try
    if FOrmDataSet.Active then
       FOrmDataSet.Close;
    FOrmDataSet.CreateDataSet;
    inherited Open(ASQL, AID);
  finally
    FOrmDataSet.First;
//    FOrmDataSet.EnableConstraints;
    FOrmDataSet.EnableControls;
    EnableDataSetEvents;
    FConnection.Disconnect;
  end;
end;

procedure TClientDataSetAdapter<M>.SetDataSetEvents;
begin
  inherited;
  FOrmDataSet.BeforeApplyUpdates := DoBeforeApplyUpdates;
  FOrmDataSet.AfterApplyUpdates  := DoAfterApplyUpdates;
end;

/// <summary>
/// Essa rotina requer na clausula USES a unit ormbr.mapping.helper, no D2010
/// ao adicionar a unit na clausula USES da classe m�e deu um error internal
//  por isso essa rotina foi descrita nas duas subclass, a onde � error n�o ocorreu.
/// </summary>
procedure TClientDataSetAdapter<M>.ExecuteCheckNotNull;
var
  oProperty: TRttiProperty;
  oRestriction: TCustomAttribute;
  iFieldIndex: Integer;
begin
   for oProperty in TObject(FCurrentRecord).GetColumns do
   begin
     iFieldIndex := (oProperty as TRttiInstanceProperty).Index + 1;
     /// Required the restriction
     oRestriction := TObject(FCurrentRecord).GetRestriction(oProperty);
     if oRestriction <> nil then
       if NotNull in Restrictions(oRestriction).Restrictions then
         if FOrmDataSet.Fields[iFieldIndex].Value = Null then
           raise EFieldNotNull.Create(oProperty.Name);
   end;
end;

end.
