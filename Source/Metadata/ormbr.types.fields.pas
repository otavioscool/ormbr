{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(12 Out 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.types.fields;

interface

uses
  DB,
  SysUtils,
  StrUtils,
  Generics.Collections,
  ormbr.types.database,
  ormbr.database.mapping;

type
  TFieldTypeRegister = class
  private
    FDriverName: TDriverName;
  class var
    FInstance: TFieldTypeRegister;
  private
    FFieldType: TDictionary<string, TFieldType>;
    constructor CreatePrivate;
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;
    class function GetInstance: TFieldTypeRegister;
    function GetFieldType(const ATypeName: string): TFieldType;
    procedure GetFieldTypeDefinition(AColumn: TColumnMIK);
    property DriverName: TDriverName read FDriverName write FDriverName;
  end;

implementation

{ TFieldTypeRegister }

constructor TFieldTypeRegister.Create;
begin

end;

constructor TFieldTypeRegister.CreatePrivate;
begin
  FFieldType := TDictionary<string, TFieldType>.Create();
  FFieldType.Add('BOOLEAN', ftBoolean);
  FFieldType.Add('TINYINT', ftByte);
  FFieldType.Add('SMALLINT', ftSmallint);
  FFieldType.Add('INT2', ftSmallint);
  FFieldType.Add('INTEGER', ftInteger);
  FFieldType.Add('INT', ftInteger);
  FFieldType.Add('INT4', ftInteger);
  FFieldType.Add('BIGINT', ftInteger);
  FFieldType.Add('SINGLE', ftSingle);
  FFieldType.Add('DOUBLE', ftFloat);
  FFieldType.Add('MONEY', ftBCD);
  FFieldType.Add('NUMERIC', ftCurrency);
  FFieldType.Add('DECIMAL', ftBCD);
  FFieldType.Add('FLOAT', ftFloat);
  FFieldType.Add('DATETIME', ftDateTime);
  FFieldType.Add('TIME', ftTime);
  FFieldType.Add('DATE', ftDate);
  FFieldType.Add('TIMESTAMP', ftTimeStamp);
  FFieldType.Add('CHAR', ftFixedChar);
  FFieldType.Add('ENUM', ftFixedChar);
  FFieldType.Add('REAL', ftExtended);
  FFieldType.Add('DOUBLE PRECISION', ftExtended);
  FFieldType.Add('VARCHAR', ftString);
  FFieldType.Add('NCHAR', ftFixedWideChar);
  FFieldType.Add('NVARCHAR', ftWideString);
  FFieldType.Add('BLOB', ftBlob);
  FFieldType.Add('BLOB SUB_TYPE TEXT', ftBlob);
  FFieldType.Add('LONGBLOB', ftMemo);
  FFieldType.Add('TEXT', ftWideMemo);
  FFieldType.Add('GUID', ftGuid);
end;

destructor TFieldTypeRegister.Destroy;
begin
  FFieldType.Free;
  inherited;
end;

procedure TFieldTypeRegister.GetFieldTypeDefinition(AColumn: TColumnMIK);
begin
  case AColumn.FieldType of
    ftByte, ftShortint, ftSmallint, ftWord:
    begin
      AColumn.TypeName := 'SMALLINT';
    end;
    ftInteger, ftLongWord:
    begin
      if      FDriverName = dnMSSQL then AColumn.TypeName := 'INT'
      else if FDriverName = dnMySQL then AColumn.TypeName := 'INT'
      else                               AColumn.TypeName := 'INTEGER';
    end;
    ftLargeint:      AColumn.TypeName := 'NUMERIC(%l)';
    ftString:        AColumn.TypeName := 'VARCHAR(%l)';
    ftWideString:    AColumn.TypeName := 'NVARCHAR(%l)';
    ftFixedChar:     AColumn.TypeName := 'CHAR(%l)';
    ftFixedWideChar: AColumn.TypeName := 'NCHAR(%l)';
    ftDate:          AColumn.TypeName := 'DATE';
    ftTime:          AColumn.TypeName := 'TIME';
    ftDateTime:      AColumn.TypeName := 'DATETIME';
    ftTimeStamp:     AColumn.TypeName := 'TIMESTAMP';
    ftFloat:
    begin
      if      FDriverName = dnMSSQL      then AColumn.TypeName := 'FLOAT'
      else if FDriverName = dnMySQL      then AColumn.TypeName := 'FLOAT'
      else if FDriverName = dnPostgreSQL then AColumn.TypeName := 'NUMERIC(%p,%s)'
      else                                    AColumn.TypeName := 'DECIMAL(%p,%s)';
    end;
    ftSingle:        AColumn.TypeName := 'REAL';
    ftExtended:
    begin
      if      FDriverName = dnMSSQL then AColumn.TypeName := 'DOUBLE'
      else if FDriverName = dnMySQL then AColumn.TypeName := 'DOUBLE'
      else                               AColumn.TypeName := 'DOUBLE PRECISION';
    end;
    ftCurrency:
    begin
      if      FDriverName = dnMSSQL      then AColumn.TypeName := 'MONEY'
      else if FDriverName = dnMySQL      then AColumn.TypeName := 'DECIMAL(%p,%s)'
      else                                    AColumn.TypeName := 'NUMERIC(%p,%s)';
    end;
    ftBCD, ftFMTBcd:
    begin
      if      FDriverName = dnPostgreSQL then AColumn.TypeName := 'MONEY'
      else if FDriverName = dnMSSQL      then AColumn.TypeName := 'MONEY'
      else                                    AColumn.TypeName := 'DECIMAL(%p,%s)';
    end;
    ftMemo:
    begin
      if FDriverName = dnFirebird then AColumn.TypeName := 'BLOB SUB_TYPE TEXT'
      else                             AColumn.TypeName := 'LOGBLOB';
    end;
    ftWideMemo, ftBlob:
    begin
      if FDriverName = dnFirebird then AColumn.TypeName := 'BLOB SUB_TYPE TEXT'
      else                             AColumn.TypeName := 'TEXT';
    end;
  else
    raise Exception.Create('Tipo da coluna definida, n�o existe no ORMBr.');
  end;
  /// <summary>
  /// Defini��es de propriedades de tamnanho
  /// </summary>
  if MatchStr(AColumn.TypeName, ['SMALLINT','INT','INTEGER','DATE','TIME',
                                 'DATETIME','TIMESTAMP','REAL','DOUBLE PRECISION',
                                 'BLOB SUB_TYPE TEXT','TEXT']) then
  begin
    AColumn.Size := 0;
    AColumn.Precision := 0;
    AColumn.Scale := 0;
  end
  else
  if MatchStr(AColumn.TypeName, ['NUMERIC(%l)','VARCHAR(%l)','NVARCHAR(%l)','CHAR(%l)','NCHAR(%l)']) then
  begin
    AColumn.Precision := 0;
    AColumn.Scale := 0;
  end
  else
  if MatchStr(AColumn.TypeName, ['DECIMAL(%p,%s)','NUMERIC(%p,%s)']) then
  begin
    AColumn.Size := 0;
  end;
end;

function TFieldTypeRegister.GetFieldType(const ATypeName: string): TFieldType;
begin
  Result := FFieldType[ATypeName];
end;

class function TFieldTypeRegister.GetInstance: TFieldTypeRegister;
begin
   if not Assigned(FInstance) then
      FInstance := TFieldTypeRegister.CreatePrivate;

   Result := FInstance;
end;

initialization

finalization
   if Assigned(TFieldTypeRegister.FInstance) then
   begin
      TFieldTypeRegister.FInstance.Free;
   end;

end.

