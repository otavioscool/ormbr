{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.database.manager;

interface

uses
  SysUtils,
  Generics.Collections,
  ormbr.database.abstract,
  ormbr.factory.interfaces,
  ormbr.database.mapping,
  ormbr.ddl.commands;

type
  TDatabaseManager = class(TDatabaseAbstract)
  private
    procedure CompareTables(AMasterDB, ATargetDB: TCatalogMetadataMIK);
    procedure CompareViews(AMasterDB, ATargetDB: TCatalogMetadataMIK);
    procedure CompareSequences(AMasterDB, ATargetDB: TCatalogMetadataMIK);
    procedure CompareColumns(AMasterTable, ATargetTable: TTableMIK);
    procedure ComparePrimaryKey(AMasterTable, ATargetTable: TTableMIK);
    procedure CompareForeignKeys(AMasterTable, ATargetTable: TTableMIK);
    procedure CompareIndexes(AMasterTable, ATargetTable: TTableMIK);
    procedure CompareTriggers(AMasterTable, ATargetTable: TTableMIK);
    procedure CompareChecks(AMasterTable, ATargetTable: TTableMIK);
    procedure ActionCreateTable(ATable: TTableMIK);
    procedure ActionCreateIndexe(AIndexe: TIndexeKeyMIK);
    procedure ActionCreateCheck(ACheck: TCheckMIK);
    procedure ActionCreatePrimaryKey(APrimaryKey: TPrimaryKeyMIK);
    procedure ActionCreateColumn(AColumn: TColumnMIK);
    procedure ActionCreateSequence(ASequence: TSequenceMIK);
    procedure ActionCreateForeignKey(AForeignKey: TForeignKeyMIK);
    procedure ActionDropTable(ATable: TTableMIK);
    procedure ActionDropColumn(AColumn: TColumnMIK);
    procedure ActionDropPrimaryKey(APrimaryKey: TPrimaryKeyMIK);
    procedure ActionDropSequence(ASequence: TSequenceMIK);
    procedure ActionDropIndexe(AIndexe: TIndexeKeyMIK);
    procedure ActionDropForeignKey(AForeignKey: TForeignKeyMIK);
    procedure ActionDropCheck(ACheck: TCheckMIK);
    procedure ActionAlterColumn(AColumn: TColumnMIK);
    procedure ActionEnableForeignKeys(AEnable: Boolean);
    procedure ActionEnableTriggers(AEnable: Boolean);
    function DeepEqualsColumn(AMasterColumn, ATargetColumn: TColumnMIK): Boolean;
    function DeepEqualsForeignKey(AMasterForeignKey, ATargetForeignKey: TForeignKeyMIK): Boolean;
    function DeepEqualsForeignKeyFromColumns(AMasterForeignKey, ATargetForeignKey: TForeignKeyMIK): Boolean;
    function DeepEqualsForeignKeyToColumns(AMasterForeignKey, ATargetForeignKey: TForeignKeyMIK): Boolean;
    function DeepEqualsIndexe(AMasterIndexe, ATargetIndexe: TIndexeKeyMIK): Boolean;
    function DeepEqualsIndexeColumns(AMasterIndexe, ATargetIndexe: TIndexeKeyMIK): Boolean;
  protected
    procedure ExtractDatabase; override;
    procedure GenerateDDLCommands(AMasterDB, ATargetDB: TCatalogMetadataMIK); override;
    procedure ExecuteDDLCommands;
  public
    constructor Create(AConnection: IDBConnection); override;
    destructor Destroy; override;
    procedure BuildDatabase; override;
  end;

implementation

{ TDatabaseManager }

procedure TDatabaseManager.BuildDatabase;
begin
  inherited;
  /// <summary>
  /// Extrai o metadata com base nos modelos existentes e no banco de dados
  /// </summary>
  ExtractDatabase;
  /// <summary>
  /// Gera os comandos DDL para atualiza��o do banco da dados.
  /// </summary>
  GenerateDDLCommands(FCatalogMaster, FCatalogTarget);
end;

constructor TDatabaseManager.Create(AConnection: IDBConnection);
begin
  inherited Create(AConnection);
end;

function TDatabaseManager.DeepEqualsColumn(AMasterColumn, ATargetColumn: TColumnMIK): Boolean;
begin
  Result := True;
  if AMasterColumn.Position <> ATargetColumn.Position then
    Exit(False);
  if AMasterColumn.FieldType <> ATargetColumn.FieldType then
    Exit(False);
  if AMasterColumn.TypeName <> ATargetColumn.TypeName then
    Exit(False);
  if AMasterColumn.Size <> ATargetColumn.Size then
    Exit(False);
  if AMasterColumn.Precision <> ATargetColumn.Precision then
    Exit(False);
  if AMasterColumn.NotNull <> ATargetColumn.NotNull then
    Exit(False);
  if AMasterColumn.AutoIncrement <> ATargetColumn.AutoIncrement then
    Exit(False);
  if AMasterColumn.SortingOrder <> ATargetColumn.SortingOrder then
    Exit(False);
  if AMasterColumn.DefaultValue <> ATargetColumn.DefaultValue then
    Exit(False);
//  if AMasterColumn.Description <> ATargetColumn.Description then
//    Exit(False);
end;

function TDatabaseManager.DeepEqualsForeignKey(AMasterForeignKey, ATargetForeignKey: TForeignKeyMIK): Boolean;
begin
  Result := True;
  if AMasterForeignKey.FromTable <> ATargetForeignKey.FromTable then
    Exit(False);
  if AMasterForeignKey.OnDelete <> ATargetForeignKey.OnDelete then
    Exit(False);
  if AMasterForeignKey.OnUpdate <> ATargetForeignKey.OnUpdate then
    Exit(False);
//  if AMasterForeignKey.Description <> ATargetForeignKey.Description then
//    Exit(False);
end;

function TDatabaseManager.DeepEqualsIndexe(AMasterIndexe, ATargetIndexe: TIndexeKeyMIK): Boolean;
begin
  Result := True;
  if AMasterIndexe.Unique <> ATargetIndexe.Unique then
    Exit(False);
//  if AMasterIndexe.Description <> ATargetIndexe.Description then
//    Exit(False);
end;

destructor TDatabaseManager.Destroy;
begin

  inherited;
end;

procedure TDatabaseManager.ExecuteDDLCommands;
var
  oCommand: TDDLCommand;
  sCommand: string;
begin
  FConnection.StartTransaction;
  try
    for oCommand in FDDLCommands do
    begin
      sCommand := oCommand.BuildCommand(FGeneratorCommand);
      if Length(sCommand) > 0 then
        if FCommandsAutoExecute then
          FConnection.ExecuteScript(sCommand);
    end;
    FConnection.Commit;
  except
    on E: Exception do
    begin
      FConnection.Rollback;
      raise Exception.Create('ORMBr Command : [' + oCommand.Warning + '] - ' + E.Message);
    end;
  end;
end;

procedure TDatabaseManager.ExtractDatabase;
begin
  inherited;
  /// <summary>
  /// Extrai todo metadata com base banco de dados acessado
  /// </summary>
  FMetadataDB.ExtractMetadata;
  /// <summary>
  /// Extrai todo metadata com base nos modelos existentes
  /// </summary>
  FMetadataOBJ.ExtractMetadata;
end;

procedure TDatabaseManager.GenerateDDLCommands(AMasterDB, ATargetDB: TCatalogMetadataMIK);
begin
  inherited;
  FDDLCommands.Clear;
  /// <summary>
  /// Gera script que desabilita todas as ForeignKeys
  /// </summary>
  ActionEnableForeignKeys(False);
  /// <summary>
  /// Gera script que desabilita todas as Triggers
  /// </summary>
  ActionEnableTriggers(False);
  /// <summary>
  /// Compara Tabelas
  /// </summary>
  CompareTables(AMasterDB, ATargetDB);
  /// <summary>
  /// Compara Views
  /// </summary>
  CompareViews(AMasterDB, ATargetDB);
  /// <summary>
  /// Compara Sequences
  /// </summary>
  CompareSequences(AMasterDB, ATargetDB);
  /// <summary>
  /// Gera script que habilita todas as ForeignKeys
  /// </summary>
  ActionEnableForeignKeys(True);
  /// <summary>
  /// Gera script que habilita todas as Triggers
  /// </summary>
  ActionEnableTriggers(True);
  /// <summary>
  /// Execute Commands
  /// </summary>
  ExecuteDDLCommands;
end;

procedure TDatabaseManager.CompareTables(AMasterDB, ATargetDB: TCatalogMetadataMIK);
var
  oTableMaster: TPair<string, TTableMIK>;
  oTableTarget: TPair<string, TTableMIK>;
begin
  /// <summary>
  /// Gera script de exclus�o de tabela, caso n�o exista um modelo para ela no banco.
  /// </summary>
  for oTableTarget in ATargetDB.Tables do
  begin
    if not AMasterDB.Tables.ContainsKey(oTableTarget.Key) then
      ActionDropTable(oTableTarget.Value);
  end;
  /// <summary>
  /// Gera script de cria��o de tabela, caso a tabela do modelo n�o exista no banco.
  /// </summary>
  for oTableMaster in AMasterDB.Tables do
  begin
    if ATargetDB.Tables.ContainsKey(oTableMaster.Key) then
    begin
      /// <summary>
      /// Table Columns
      /// </summary>
      CompareColumns(oTableMaster.Value, ATargetDB.Tables.Items[oTableMaster.Key]);

      /// <summary>
      /// Table PrimaryKey
      /// </summary>
      if (oTableMaster.Value.PrimaryKey.Fields.Count > 0) or
         (ATargetDB.Tables.Items[oTableMaster.Key].PrimaryKey.Fields.Count > 0) then
        ComparePrimaryKey(oTableMaster.Value, ATargetDB.Tables.Items[oTableMaster.Key]);

      /// <summary>
      /// Table Indexes
      /// </summary>
      if (oTableMaster.Value.IndexeKeys.Count > 0) or
         (ATargetDB.Tables.Items[oTableMaster.Key].IndexeKeys.Count > 0) then
        CompareIndexes(oTableMaster.Value, ATargetDB.Tables.Items[oTableMaster.Key]);

      /// <summary>
      /// Table ForeignKeys
      /// </summary>
      if (oTableMaster.Value.ForeignKeys.Count > 0) or
         (ATargetDB.Tables.Items[oTableMaster.Key].ForeignKeys.Count > 0) then
      CompareForeignKeys(oTableMaster.Value, ATargetDB.Tables.Items[oTableMaster.Key]);

      /// <summary>
      /// Table Checks
      /// </summary>
      if (oTableMaster.Value.Checks.Count > 0) or
         (ATargetDB.Tables.Items[oTableMaster.Key].Checks.Count > 0) then
        CompareChecks(oTableMaster.Value, ATargetDB.Tables.Items[oTableMaster.Key]);

      /// <summary>
      /// Table Triggers
      /// </summary>
      if (oTableMaster.Value.Triggers.Count > 0) or
         (ATargetDB.Tables.Items[oTableMaster.Key].Triggers.Count > 0) then
      CompareTriggers(oTableMaster.Value, ATargetDB.Tables.Items[oTableMaster.Key]);
    end
    else
      ActionCreateTable(oTableMaster.Value);
  end;
end;

procedure TDatabaseManager.CompareTriggers(AMasterTable, ATargetTable: TTableMIK);
var
  oTriggerMaster: TPair<string, TTriggerMIK>;
  oTriggerTarget: TPair<string, TTriggerMIK>;
begin
  /// <summary>
  /// Remove trigger que n�o existe no modelo.
  /// </summary>
  for oTriggerTarget in ATargetTable.Triggers do
  begin
    if not AMasterTable.Triggers.ContainsKey(oTriggerTarget.Key) then
//      ActionDropTrigger(oTriggerTarget.Value);
  end;
  /// <summary>
  /// Gera script de cria��o de trigger, caso a trigger do modelo n�o exista no banco.
  /// </summary>
  for oTriggerMaster in AMasterTable.Triggers do
  begin
    if ATargetTable.Triggers.ContainsKey(oTriggerMaster.Key) then
//      CompareTriggerScript(oTriggerMaster.Value, ATargetTable.Triggers.Items[oTriggerMaster.Key])
    else
//      ActionCreateTrigger(oTriggerMaster.Value);
  end;
end;

procedure TDatabaseManager.CompareViews(AMasterDB, ATargetDB: TCatalogMetadataMIK);
var
  oViewMaster: TPair<string, TViewMIK>;
  oViewTarget: TPair<string, TViewMIK>;
begin
  /// <summary>
  /// Gera script de exclus�o da view, caso n�o exista um modelo para ela no banco.
  /// </summary>
  for oViewTarget in ATargetDB.Views do
  begin
    if not AMasterDB.Views.ContainsKey(oViewTarget.Key) then
//      ActionDropView(oViewTarget.Value);
  end;
  /// <summary>
  /// Gera script de cria��o da view, caso a view do modelo n�o exista no banco.
  /// </summary>
  for oViewMaster in AMasterDB.Views do
  begin
    if ATargetDB.Views.ContainsKey(oViewMaster.Key) then
//      CompareScript(oViewMaster.Value, ATargetDB.Views.Items[oViewMaster.Key])
    else
//      ActionCreateView(oViewMaster.Value);
  end;
end;

procedure TDatabaseManager.CompareChecks(AMasterTable, ATargetTable: TTableMIK);
begin

end;

procedure TDatabaseManager.CompareColumns(AMasterTable, ATargetTable: TTableMIK);
var
  oColumnMaster: TPair<string, TColumnMIK>;
  oColumnTarget: TPair<string, TColumnMIK>;
  oColumn: TColumnMIK;

  function ExistMasterColumn(AColumnName: string): TColumnMIK;
  var
    oColumn: TPair<string, TColumnMIK>;
  begin
    Result := nil;
    for oColumn in AMasterTable.Fields do
      if SameText(oColumn.Value.Name, AColumnName) then
        Exit(oColumn.Value);
  end;

  function ExistTargetColumn(AColumnName: string): TColumnMIK;
  var
    oColumn: TPair<string, TColumnMIK>;
  begin
    Result := nil;
    for oColumn in ATargetTable.Fields do
      if SameText(oColumn.Value.Name, AColumnName) then
        Exit(oColumn.Value);
  end;

begin
  /// <summary>
  /// Remove coluna que n�o existe no modelo.
  /// </summary>
  for oColumnTarget in ATargetTable.FieldsSort do
  begin
    oColumn := ExistMasterColumn(oColumnTarget.Value.Name);
    if oColumn = nil then
      ActionDropColumn(oColumnTarget.Value);
  end;
  /// <summary>
  /// Adiciona coluna do modelo que n�o exista no banco
  /// Compara coluna que exista no modelo e no banco
  /// </summary>
  for oColumnMaster in AMasterTable.FieldsSort do
  begin
    oColumn := ExistTargetColumn(oColumnMaster.Value.Name);
    if oColumn = nil then
      ActionCreateColumn(oColumnMaster.Value)
    else
    begin
      if not DeepEqualsColumn(oColumnMaster.Value, oColumn) then
        ActionAlterColumn(oColumnMaster.Value);
    end;
  end;
end;

function TDatabaseManager.DeepEqualsForeignKeyFromColumns(AMasterForeignKey, ATargetForeignKey: TForeignKeyMIK): Boolean;
var
  oColumnMaster: TPair<string, TColumnMIK>;
  oColumnTarget: TPair<string, TColumnMIK>;
  oColumn: TColumnMIK;

  function ExistMasterFromColumn(AColumnName: string): TColumnMIK;
  var
    oColumn: TPair<string, TColumnMIK>;
  begin
    Result := nil;
    for oColumn in AMasterForeignKey.FromFields do
      if SameText(oColumn.Value.Name, AColumnName) then
        Exit(oColumn.Value);
  end;

  function ExistTargetFromColumn(AColumnName: string): TColumnMIK;
  var
    oColumn: TPair<string, TColumnMIK>;
  begin
    Result := nil;
    for oColumn in ATargetForeignKey.FromFields do
      if SameText(oColumn.Value.Name, AColumnName) then
        Exit(oColumn.Value);
  end;

begin
  Result := True;
  /// <summary>
  /// Compara��o dos campos dos indexes banco/modelo
  /// </summary>
  for oColumnTarget in ATargetForeignKey.FromFieldsSort do
  begin
    oColumn := ExistMasterFromColumn(oColumnTarget.Value.Name);
    if oColumn = nil then
      Exit(False)
  end;
  /// <summary>
  /// Compara��o dos campos dos indexes modelo/banco
  /// </summary>
  for oColumnMaster in AMasterForeignKey.FromFieldsSort do
  begin
    oColumn := ExistTargetFromColumn(oColumnMaster.Value.Name);
    if oColumn = nil then
      Exit(False)
    else
    begin
      if not DeepEqualsColumn(oColumnMaster.Value, oColumn) then
        Exit(False);
    end;
  end;
end;

procedure TDatabaseManager.CompareForeignKeys(AMasterTable, ATargetTable: TTableMIK);
var
  oForeignKeyMaster: TPair<string, TForeignKeyMIK>;
  oForeignKeyTarget: TPair<string, TForeignKeyMIK>;
begin
  /// <summary>
  /// Remove indexe que n�o existe no modelo.
  /// </summary>
  for oForeignKeyTarget in ATargetTable.ForeignKeys do
  begin
    if not AMasterTable.ForeignKeys.ContainsKey(oForeignKeyTarget.Key) then
      ActionDropForeignKey(oForeignKeyTarget.Value);
  end;
  /// <summary>
  /// Gera script de cria��o de indexe, caso a indexe do modelo n�o exista no banco.
  /// </summary>
  for oForeignKeyMaster in AMasterTable.ForeignKeys do
  begin
    if ATargetTable.ForeignKeys.ContainsKey(oForeignKeyMaster.Key) then
    begin
      /// <summary>
      /// Checa diferen�a do ForeignKey
      /// </summary>
      if (not DeepEqualsForeignKey(oForeignKeyMaster.Value, oForeignKeyTarget.Value)) or
         (not DeepEqualsForeignKeyFromColumns(oForeignKeyMaster.Value, ATargetTable.ForeignKeys.Items[oForeignKeyMaster.Key])) or
         (not DeepEqualsForeignKeyToColumns  (oForeignKeyMaster.Value, ATargetTable.ForeignKeys.Items[oForeignKeyMaster.Key])) then
      begin
        ActionDropForeignKey(oForeignKeyTarget.Value);
        ActionCreateForeignKey(oForeignKeyMaster.Value);
      end;
    end
    else
      ActionCreateForeignKey(oForeignKeyMaster.Value);
  end;
end;

function TDatabaseManager.DeepEqualsForeignKeyToColumns(AMasterForeignKey, ATargetForeignKey: TForeignKeyMIK): Boolean;
var
  oColumnMaster: TPair<string, TColumnMIK>;
  oColumnTarget: TPair<string, TColumnMIK>;
  oColumn: TColumnMIK;

  function ExistMasterToColumn(AColumnName: string): TColumnMIK;
  var
    oColumn: TPair<string, TColumnMIK>;
  begin
    Result := nil;
    for oColumn in AMasterForeignKey.ToFields do
      if SameText(oColumn.Value.Name, AColumnName) then
        Exit(oColumn.Value);
  end;

  function ExistTargetFromColumn(AColumnName: string): TColumnMIK;
  var
    oColumn: TPair<string, TColumnMIK>;
  begin
    Result := nil;
    for oColumn in ATargetForeignKey.ToFields do
      if SameText(oColumn.Value.Name, AColumnName) then
        Exit(oColumn.Value);
  end;

begin
  /// <summary>
  /// Compara��o dos campos dos indexes banco/modelo
  /// </summary>
  for oColumnTarget in ATargetForeignKey.ToFieldsSort do
  begin
    oColumn := ExistMasterToColumn(oColumnTarget.Value.Name);
    if oColumn = nil then
      Exit(False)
  end;
  /// <summary>
  /// Compara��o dos campos dos indexes modelo/banco
  /// </summary>
  for oColumnMaster in AMasterForeignKey.ToFieldsSort do
  begin
    oColumn := ExistTargetFromColumn(oColumnMaster.Value.Name);
    if oColumn = nil then
      Exit(False)
    else
    begin
      if not DeepEqualsColumn(oColumnMaster.Value, oColumn) then
        Exit(False);
    end;
  end;
end;

function TDatabaseManager.DeepEqualsIndexeColumns(AMasterIndexe, ATargetIndexe: TIndexeKeyMIK): Boolean;
var
  oColumnMaster: TPair<string, TColumnMIK>;
  oColumnTarget: TPair<string, TColumnMIK>;
  oColumn: TColumnMIK;

  function ExistMasterColumn(AColumnName: string): TColumnMIK;
  var
    oColumn: TPair<string, TColumnMIK>;
  begin
    Result := nil;
    for oColumn in AMasterIndexe.Fields do
      if SameText(oColumn.Value.Name, AColumnName) then
        Exit(oColumn.Value);
  end;

  function ExistTargetColumn(AColumnName: string): TColumnMIK;
  var
    oColumn: TPair<string, TColumnMIK>;
  begin
    Result := nil;
    for oColumn in ATargetIndexe.Fields do
      if SameText(oColumn.Value.Name, AColumnName) then
        Exit(oColumn.Value);
  end;

begin
  Result := True;
  /// <summary>
  /// Compara��o dos campos dos indexes banco/modelo
  /// </summary>
  for oColumnTarget in ATargetIndexe.FieldsSort do
  begin
    oColumn := ExistMasterColumn(oColumnTarget.Value.Name);
    if oColumn = nil then
      Exit(False)
  end;
  /// <summary>
  /// Compara��o dos campos dos indexes modelo/banco
  /// </summary>
  for oColumnMaster in AMasterIndexe.FieldsSort do
  begin
    oColumn := ExistTargetColumn(oColumnMaster.Value.Name);
    if oColumn = nil then
      Exit(False)
    else
    begin
      if not DeepEqualsColumn(oColumnMaster.Value, oColumn) then
        Exit(False);
    end;
  end;
end;

procedure TDatabaseManager.CompareIndexes(AMasterTable, ATargetTable: TTableMIK);
var
  oIndexeMaster: TPair<string, TIndexeKeyMIK>;
  oIndexeTarget: TPair<string, TIndexeKeyMIK>;
begin
  /// <summary>
  /// Remove indexe que n�o existe no modelo.
  /// </summary>
  for oIndexeTarget in ATargetTable.IndexeKeys do
  begin
    if not AMasterTable.IndexeKeys.ContainsKey(oIndexeTarget.Key) then
      ActionDropIndexe(oIndexeTarget.Value);
  end;
  /// <summary>
  /// Gera script de cria��o de indexe, caso a indexe do modelo n�o exista no banco.
  /// </summary>
  for oIndexeMaster in AMasterTable.IndexeKeys do
  begin
    if ATargetTable.IndexeKeys.ContainsKey(oIndexeMaster.Key) then
    begin
      if (not DeepEqualsIndexe(oIndexeMaster.Value, oIndexeTarget.Value)) or
         (not DeepEqualsIndexeColumns(oIndexeMaster.Value, ATargetTable.IndexeKeys.Items[oIndexeMaster.Key])) then
      begin
        ActionDropIndexe(oIndexeTarget.Value);
        ActionCreateIndexe(oIndexeMaster.Value);
      end;
    end
    else
      ActionCreateIndexe(oIndexeMaster.Value);
  end;
end;

procedure TDatabaseManager.ComparePrimaryKey(AMasterTable, ATargetTable: TTableMIK);
var
  oColumnMaster: TPair<string, TColumnMIK>;
  oColumn: TColumnMIK;

  function ExistTargetColumn(AColumnName: string): TColumnMIK;
  var
    oColumn: TPair<string, TColumnMIK>;
  begin
    Result := nil;
    for oColumn in ATargetTable.PrimaryKey.Fields do
      if SameText(oColumn.Value.Name, AColumnName) then
        Exit(oColumn.Value);
  end;

begin
  if not SameText(AMasterTable.PrimaryKey.Name, ATargetTable.PrimaryKey.Name) then
    ActionDropPrimaryKey(ATargetTable.PrimaryKey);

  /// <summary>
  /// Se alguma coluna n�o existir na PrimaryKey do banco recria a PrimaryKey.
  /// </summary>
  for oColumnMaster in AMasterTable.PrimaryKey.FieldsSort do
  begin
    oColumn := ExistTargetColumn(oColumnMaster.Value.Name);
    if oColumn = nil then
    begin
      ActionDropPrimaryKey(ATargetTable.PrimaryKey);
      ActionCreatePrimaryKey(AMasterTable.PrimaryKey);
    end;
  end;
end;

procedure TDatabaseManager.CompareSequences(AMasterDB, ATargetDB: TCatalogMetadataMIK);
var
  oSequenceMaster: TPair<string, TSequenceMIK>;
  oSequenceTarget: TPair<string, TSequenceMIK>;
begin
  /// <summary>
  /// Checa se existe alguma sequence no banco, da qual n�o exista nos modelos
  /// para exclus�o da mesma.
  /// </summary>
  for oSequenceTarget in ATargetDB.Sequences do
  begin
    if not AMasterDB.Sequences.ContainsKey(oSequenceTarget.Key) then
      ActionDropSequence(oSequenceTarget.Value);
  end;
  /// <summary>
  /// Checa se existe a sequence no banco, se n�o existir cria se existir.
  /// </summary>
  for oSequenceMaster in AMasterDB.Sequences do
  begin
    if not ATargetDB.Sequences.ContainsKey(oSequenceMaster.Key) then
      ActionCreateSequence(oSequenceMaster.Value);
  end;
end;

procedure TDatabaseManager.ActionAlterColumn(AColumn: TColumnMIK);
begin
  FDDLCommands.Add(TDDLCommandAlterColumn.Create(AColumn));
end;

procedure TDatabaseManager.ActionCreateCheck(ACheck: TCheckMIK);
begin
  FDDLCommands.Add(TDDLCommandCreateCheck.Create(ACheck));
end;

procedure TDatabaseManager.ActionCreateColumn(AColumn: TColumnMIK);
begin
  FDDLCommands.Add(TDDLCommandCreateColumn.Create(AColumn));
end;

procedure TDatabaseManager.ActionCreateForeignKey(AForeignKey: TForeignKeyMIK);
begin
  FDDLCommands.Add(TDDLCommandCreateForeignKey.Create(AForeignKey));
end;

procedure TDatabaseManager.ActionCreateIndexe(AIndexe: TIndexeKeyMIK);
begin
  FDDLCommands.Add(TDDLCommandCreateIndexe.Create(AIndexe));
end;

procedure TDatabaseManager.ActionCreatePrimaryKey(APrimaryKey: TPrimaryKeyMIK);
begin
  FDDLCommands.Add(TDDLCommandCreatePrimaryKey.Create(APrimaryKey));
end;

procedure TDatabaseManager.ActionCreateSequence(ASequence: TSequenceMIK);
begin
  FDDLCommands.Add(TDDLCommandCreateSequence.Create(ASequence));
end;

procedure TDatabaseManager.ActionCreateTable(ATable: TTableMIK);
begin
  FDDLCommands.Add(TDDLCommandCreateTable.Create(ATable));
end;

procedure TDatabaseManager.ActionDropCheck(ACheck: TCheckMIK);
begin
  FDDLCommands.Add(TDDLCommandDropCheck.Create(ACheck));
end;

procedure TDatabaseManager.ActionDropColumn(AColumn: TColumnMIK);
begin
  FDDLCommands.Add(TDDLCommandDropColumn.Create(AColumn));
end;

procedure TDatabaseManager.ActionDropForeignKey(AForeignKey: TForeignKeyMIK);
begin
  FDDLCommands.Add(TDDLCommandDropForeignKey.Create(AForeignKey));
end;

procedure TDatabaseManager.ActionDropIndexe(AIndexe: TIndexeKeyMIK);
begin
  FDDLCommands.Add(TDDLCommandDropIndexe.Create(AIndexe));
end;

procedure TDatabaseManager.ActionDropPrimaryKey(APrimaryKey: TPrimaryKeyMIK);
begin
  FDDLCommands.Add(TDDLCommandDropPrimaryKey.Create(APrimaryKey));
end;

procedure TDatabaseManager.ActionDropSequence(ASequence: TSequenceMIK);
begin
  FDDLCommands.Add(TDDLCommandDropSequence.Create(ASequence));
end;

procedure TDatabaseManager.ActionDropTable(ATable: TTableMIK);
begin
  FDDLCommands.Add(TDDLCommandDropTable.Create(ATable));
end;

procedure TDatabaseManager.ActionEnableForeignKeys(AEnable: Boolean);
begin
  FDDLCommands.Add(TDDLCommandEnableForeignKeys.Create(AEnable));
end;

procedure TDatabaseManager.ActionEnableTriggers(AEnable: Boolean);
begin
  FDDLCommands.Add(TDDLCommandEnableTriggers.Create(AEnable));
end;

end.
