{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(12 Out 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.ddl.interfaces;

interface

uses
  ormbr.database.mapping;

type
  /// <summary>
  /// Class unit : ormbr.ddl.generator.pas
  /// Class Name : TDDLSQLGeneratorAbstract
  /// </summary>
  IDDLGeneratorCommand = interface
    ['{3FFFA9FB-9318-4D40-9C3A-ACC38F0F4C19}']
    function GenerateCreateTable(ATable: TTableMIK): string;
    function GenerateCreatePrimaryKey(APrimaryKey: TPrimaryKeyMIK): string;
    function GenerateCreateForeignKey(AForeignKey: TForeignKeyMIK): string;
    function GenerateCreateSequence(ASequence: TSequenceMIK): string;
    function GenerateCreateIndexe(AIndexe: TIndexeKeyMIK): string;
    function GenerateCreateCheck(ACheck: TCheckMIK): string;
    function GenerateCreateView(AView: TViewMIK): string;
    function GenerateCreateTrigger(ATrigger: TTriggerMIK): string;
    function GenerateCreateColumn(AColumn: TColumnMIK): string;
    function GenerateAlterColumn(AColumn: TColumnMIK): string;
    function GenerateDropTable(ATable: TTableMIK): string;
    function GenerateDropPrimaryKey(APrimaryKey: TPrimaryKeyMIK): string;
    function GenerateDropForeignKey(AForeignKey: TForeignKeyMIK): string;
    function GenerateDropSequence(ASequence: TSequenceMIK): string;
    function GenerateDropIndexe(AIndexe: TIndexeKeyMIK): string;
    function GenerateDropCheck(ACheck: TCheckMIK): string;
    function GenerateDropView(AView: TViewMIK): string;
    function GenerateDropTrigger(ATrigger: TTriggerMIK): string;
    function GenerateDropColumn(AColumn: TColumnMIK): string;
    function GenerateEnableForeignKeys(AEnable: Boolean): string;
    function GenerateEnableTriggers(AEnable: Boolean): string;
  end;

implementation

end.
