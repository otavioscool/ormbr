{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.database.abstract;

interface

uses
  SysUtils,
  Generics.Collections,
  ormbr.factory.interfaces,
  ormbr.metadata.db.factory,
  ormbr.metadata.classe.factory,
  ormbr.database.mapping,
  ormbr.types.fields,
  ormbr.ddl.interfaces,
  ormbr.ddl.register,
  ormbr.ddl.commands;

type
  TDatabaseAbstract = class abstract
  protected
    FConnection: IDBConnection;
    FGeneratorCommand: IDDLGeneratorCommand;
    FDDLCommands: TList<TDDLCommand>;
    FCatalogMaster: TCatalogMetadataMIK;
    FCatalogTarget: TCatalogMetadataMIK;
    FMetadataDB: TMetadataDBAbstract;
    FMetadataOBJ: TMetadataClasseAbstract;
    FCommandsAutoExecute: Boolean;
    procedure ExtractDatabase; virtual; abstract;
    procedure GenerateDDLCommands(AMasterDB, ATargetDB: TCatalogMetadataMIK); virtual; abstract;
  public
    constructor Create(AConnection: IDBConnection); overload; virtual;
    destructor Destroy; override;
    procedure BuildDatabase; virtual; abstract;
    function GetCommandList: TArray<TDDLCommand>;
    property Connection: IDBConnection read FConnection;
    property GeneratorCommand: IDDLGeneratorCommand read FGeneratorCommand;
    property CommandsAutoExecute: Boolean read FCommandsAutoExecute write FCommandsAutoExecute;
  end;

implementation

{ TAbstractDatabase }

constructor TDatabaseAbstract.Create(AConnection: IDBConnection);
begin
  FCommandsAutoExecute := True;
  FConnection := AConnection;
  FConnection.Connect;
  if not FConnection.IsConnected then
    raise Exception.Create('N�o foi possivel fazer conex�o com o banco de dados');

  FGeneratorCommand := TSQLDriverRegister.GetInstance.GetDriver(FConnection.GetDriverName);
  FCatalogMaster := TCatalogMetadataMIK.Create;
  FCatalogTarget := TCatalogMetadataMIK.Create;
  FMetadataOBJ := TMetadataClasseFactory.Create(AConnection, FCatalogMaster);
  FMetadataDB := TMetadataDBFactory.Create(AConnection, FCatalogTarget);
  FDDLCommands := TObjectList<TDDLCommand>.Create;
  /// <summary>
  /// Define o nome do driver usado para tratamento dos tipos do campos
  /// </summary>
  TFieldTypeRegister.GetInstance.DriverName := FConnection.GetDriverName;
end;

destructor TDatabaseAbstract.Destroy;
begin
  FDDLCommands.Free;
  FMetadataDB.Free;
  FMetadataOBJ.Free;
  FCatalogMaster.Free;
  FCatalogTarget.Free;
  inherited;
end;

function TDatabaseAbstract.GetCommandList: TArray<TDDLCommand>;
var
  iIndex: Integer;
begin
  SetLength(Result, FDDLCommands.Count);
  iIndex := 0;
  for iIndex := 0 to FDDLCommands.Count - 1 do
    Result[iIndex] := FDDLCommands[iIndex];
end;

end.

