{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.metadata.model;

interface

uses
  SysUtils,
  ormbr.metadata.extract,
  ormbr.database.mapping,
  ormbr.mapping.classes,
  ormbr.mapping.explorer,
  ormbr.types.fields,
  ormbr.types.database;

type
  TModelMetadata = class(TModelMetadataAbstract)
  public
    procedure GetCatalogs; override;
    procedure GetSchemas; override;
    procedure GetTables; override;
    procedure GetColumns(ATable: TTableMIK; AClass: TClass); override;
    procedure GetPrimaryKey(ATable: TTableMIK; AClass: TClass); override;
    procedure GetIndexeKeys(ATable: TTableMIK; AClass: TClass); override;
    procedure GetForeignKeys(ATable: TTableMIK; AClass: TClass); override;
    procedure GetChecks(ATable: TTableMIK; AClass: TClass); override;
    procedure GetSequences; override;
    procedure GetProcedures; override;
    procedure GetFunctions; override;
    procedure GetViews; override;
    procedure GetTriggers; override;
    procedure GetModelMetadata; override;
  end;

implementation

{ TModelMetadata }

procedure TModelMetadata.GetModelMetadata;
begin
  GetCatalogs;
end;

procedure TModelMetadata.GetCatalogs;
begin
  FCatalogMetadata.Name := '';
  GetSchemas;
end;

procedure TModelMetadata.GetSchemas;
begin
  FCatalogMetadata.Schema := '';
  GetSequences;
  GetTables;
end;

procedure TModelMetadata.GetTables;
var
  oClass: TClass;
  oTable: TTableMIK;
  oTableMap: TTableMapping;
begin
  for oClass in TMappingExplorer.GetInstance.Repository.List.Entitys do
  begin
    oTableMap := TMappingExplorer.GetInstance.GetMappingTable(oClass);
    if oTableMap <> nil then
    begin
      oTable := TTableMIK.Create(FCatalogMetadata);
      oTable.Name := oTableMap.Name;
      oTable.Description := oTableMap.Description;
      /// <summary>
      /// Extrair colunas
      /// </summary>
      GetColumns(oTable, oClass);
      /// <summary>
      /// Extrair Primary Key
      /// </summary>
      GetPrimaryKey(oTable, oClass);
      /// <summary>
      /// Extrair Foreign Keys
      /// </summary>
      GetForeignKeys(oTable, oClass);
      /// <summary>
      /// Extrair Indexes
      /// </summary>
      GetIndexeKeys(oTable, oClass);
      /// <summary>
      /// Extrair Indexes
      /// </summary>
      GetChecks(oTable, oClass);
      /// <summary>
      /// Adiciona na lista de tabelas extraidas
      /// </summary>
      FCatalogMetadata.Tables.Add(UpperCase(oTable.Name), oTable);
    end;
  end;
end;

procedure TModelMetadata.GetChecks(ATable: TTableMIK; AClass: TClass);
var
  oCheck: TCheckMIK;
  oCheckMapList: TCheckMappingList;
  oCheckMap: TCheckMapping;
begin
  oCheckMapList := TMappingExplorer.GetInstance.GetMappingCheck(AClass);
  if oCheckMapList <> nil then
  begin
    for oCheckMap in oCheckMapList do
    begin
      oCheck := TCheckMIK.Create(ATable);
      oCheck.Name := oCheckMap.Name;
      oCheck.Condition := oCheckMap.Condition;
      oCheck.Description := '';
      ATable.Checks.Add(UpperCase(oCheck.Name), oCheck);
    end;
  end;
end;

procedure TModelMetadata.GetColumns(ATable: TTableMIK; AClass: TClass);
var
  oColumn: TColumnMIK;
  oColumnMap: TColumnMapping;
  oColumnMapList: TColumnMappingList;
begin
  oColumnMapList := TMappingExplorer.GetInstance.GetMappingColumn(AClass);
  if oColumnMapList <> nil then
  begin
    for oColumnMap in oColumnMapList do
    begin
      oColumn := TColumnMIK.Create(ATable);
      oColumn.Name := oColumnMap.Name;
      oColumn.Description := oColumnMap.Description;
      oColumn.Position := oColumnMap.FieldIndex;
      oColumn.NotNull := oColumnMap.NotNull;
      oColumn.DefaultValue := oColumnMap.DefaultValue;
      oColumn.Size := oColumnMap.Size;
      oColumn.Precision := oColumnMap.Precision;
      oColumn.Scale := oColumnMap.Scale;
      oColumn.FieldType := oColumnMap.FieldType;
      /// <summary>
      /// Resolve Field Type
      /// </summary>
      TFieldTypeRegister.GetInstance.GetFieldTypeDefinition(oColumn);
      ATable.Fields.Add(FormatFloat('000000', oColumn.Position), oColumn);
    end;
  end;
end;

procedure TModelMetadata.GetForeignKeys(ATable: TTableMIK; AClass: TClass);
var
  oForeignKey: TForeignKeyMIK;
  oForeignKeyMapList: TForeignKeyMappingList;
  oForeignKeyMap: TForeignKeyMapping;
  oFromField: TColumnMIK;
  oToField: TColumnMIK;
begin
  oForeignKeyMapList := TMappingExplorer.GetInstance.GetMappingForeignKey(AClass);
  if oForeignKeyMapList <> nil then
  begin
    for oForeignKeyMap in oForeignKeyMapList do
    begin
      oForeignKey := TForeignKeyMIK.Create(ATable);
      oForeignKey.Name := oForeignKeyMap.Name;
      oForeignKey.FromTable := oForeignKeyMap.ReferenceTableName;
      oForeignKey.OnUpdate := oForeignKeyMap.RuleUpdate;
      oForeignKey.OnDelete := oForeignKeyMap.RuleDelete;
      oForeignKey.Description := oForeignKeyMap.Description;
      ATable.ForeignKeys.Add(oForeignKey.Name, oForeignKey);
      /// <summary>
      /// Coluna tabela master
      /// </summary>
      oFromField := TColumnMIK.Create(ATable);
      oFromField.Name := oForeignKeyMap.FromColumns;
      oForeignKey.FromFields.Add(oFromField.Name, oFromField);
      /// <summary>
      /// Coluna tabela filha
      /// </summary>
      oToField := TColumnMIK.Create(ATable);
      oToField.Name := oForeignKeyMap.ToColumns;
      oForeignKey.ToFields.Add(oToField.Name, oToField);
    end;
  end;
end;

procedure TModelMetadata.GetFunctions;
begin

end;

procedure TModelMetadata.GetPrimaryKey(ATable: TTableMIK; AClass: TClass);
var
  oPrimaryKeyMap: TPrimaryKeyMapping;

  procedure GetPrimaryKeyColumns(APrimaryKey: TPrimaryKeyMIK);
  var
    oColumn: TColumnMIK;
    iFor: Integer;
  begin
    for iFor := 0 to oPrimaryKeyMap.ColumnsCount -1 do
    begin
      oColumn := TColumnMIK.Create(ATable);
      oColumn.Name := oPrimaryKeyMap.Columns[iFor];
      oColumn.Description := oPrimaryKeyMap.Description;
      oColumn.SortingOrder := oPrimaryKeyMap.SortingOrder;
      oColumn.Position := iFor;
      APrimaryKey.Fields.Add(FormatFloat('000000', oColumn.Position), oColumn);
    end;
  end;

begin
  oPrimaryKeyMap := TMappingExplorer.GetInstance.GetMappingPrimaryKey(AClass);
  if oPrimaryKeyMap <> nil then
  begin
    ATable.PrimaryKey.Name := Format('PK_%s', [ATable.Name]);
    ATable.PrimaryKey.Description := '';
    /// <summary>
    /// Estrai as columnas da primary key
    /// </summary>
    GetPrimaryKeyColumns(ATable.PrimaryKey);
  end;
end;

procedure TModelMetadata.GetProcedures;
begin

end;

procedure TModelMetadata.GetSequences;
var
  oClass: TClass;
  oSequence: TSequenceMIK;
  oSequenceMap: TSequenceMapping;
begin
  for oClass in TMappingExplorer.GetInstance.Repository.List.Entitys do
  begin
    oSequenceMap := TMappingExplorer.GetInstance.GetMappingSequence(oClass);
    if oSequenceMap <> nil then
    begin
      oSequence := TSequenceMIK.Create(FCatalogMetadata);
      oSequence.TableName := oSequenceMap.TableName;
      oSequence.Name := oSequenceMap.Name;
      oSequence.Description := oSequenceMap.Description;
      if FConnection.GetDriverName = dnMySQL then
        FCatalogMetadata.Sequences.Add(UpperCase(oSequence.TableName), oSequence)
      else
        FCatalogMetadata.Sequences.Add(UpperCase(oSequence.Name), oSequence);
    end;
  end;
end;

procedure TModelMetadata.GetTriggers;
begin

end;

procedure TModelMetadata.GetIndexeKeys(ATable: TTableMIK; AClass: TClass);
var
  oIndexeKey: TIndexeKeyMIK;
  oIndexeKeyMapList: TIndexeMappingList;
  oIndexeKeyMap: TIndexeMapping;

  procedure GetIndexeKeyColumns(AIndexeKey: TIndexeKeyMIK);
  var
    oColumn: TColumnMIK;
    iFor: Integer;
  begin
      for iFor := 0 to oIndexeKeyMap.ColumnsCount -1 do
      begin
        oColumn := TColumnMIK.Create(ATable);
        oColumn.Name := oIndexeKeyMap.Columns[iFor];
        oColumn.Description := oIndexeKeyMap.Description;
        oColumn.SortingOrder := oIndexeKeyMap.SortingOrder;
        oColumn.Position := iFor;
        AIndexeKey.Fields.Add(FormatFloat('000000', oColumn.Position), oColumn);
      end;
  end;

begin
  oIndexeKeyMapList := TMappingExplorer.GetInstance.GetMappingIndexe(AClass);
  if oIndexeKeyMapList <> nil then
  begin
    for oIndexeKeyMap in oIndexeKeyMapList do
    begin
      oIndexeKey := TIndexeKeyMIK.Create(ATable);
      oIndexeKey.Name := oIndexeKeyMap.Name;
      oIndexeKey.Unique := oIndexeKeyMap.Unique;
      oIndexeKey.Description := '';
      ATable.IndexeKeys.Add(UpperCase(oIndexeKey.Name), oIndexeKey);
      /// <summary>
      /// Estrai as columnas da indexe key
      /// </summary>
      GetIndexeKeyColumns(oIndexeKey);
    end;
  end;
end;

procedure TModelMetadata.GetViews;
begin

end;

end.
