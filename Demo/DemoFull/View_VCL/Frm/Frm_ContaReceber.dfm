inherited FrmContaReceber: TFrmContaReceber
  Caption = 'Conta a Receber'
  ClientHeight = 474
  ClientWidth = 702
  OnDestroy = FormDestroy
  ExplicitWidth = 718
  ExplicitHeight = 513
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnTitulo: TPanel
    Width = 702
    ExplicitWidth = 702
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 25
    Width = 702
    Height = 449
    ActivePage = tabCLI_Detalhe
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    ExplicitLeft = -8
    ExplicitTop = 31
    object tabCLI_Pesquisa: TTabSheet
      Caption = 'Pesquisa'
      object DBGrid1: TDBGrid
        Left = 0
        Top = 0
        Width = 694
        Height = 418
        Align = alClient
        DataSource = dsContareceber_Pesquisa
        TabOrder = 0
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -13
        TitleFont.Name = 'Arial'
        TitleFont.Style = []
      end
    end
    object tabCLI_Detalhe: TTabSheet
      Caption = 'Detalhe'
      ImageIndex = 1
      OnShow = tabCLI_DetalheShow
      object DBText1: TDBText
        Left = 143
        Top = 48
        Width = 65
        Height = 17
        DataField = 'contareceber_id'
        DataSource = dsContareceber_Detalhe
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label1: TLabel
        Left = 88
        Top = 48
        Width = 49
        Height = 16
        Caption = 'Codigo:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label2: TLabel
        Left = 86
        Top = 74
        Width = 44
        Height = 16
        Caption = 'Cliente:'
      end
      object Label3: TLabel
        Left = 32
        Top = 101
        Width = 106
        Height = 16
        Caption = 'Data Lan'#231'amento:'
      end
      object Label4: TLabel
        Left = 105
        Top = 136
        Width = 33
        Height = 16
        Caption = 'Valor:'
      end
      object Label5: TLabel
        Left = 35
        Top = 158
        Width = 103
        Height = 16
        Caption = 'Data Vencimento:'
      end
      object Label6: TLabel
        Left = 43
        Top = 195
        Width = 72
        Height = 16
        Caption = 'Observa'#231#227'o:'
      end
      object DBEdit1: TDBEdit
        Left = 144
        Top = 71
        Width = 64
        Height = 24
        DataField = 'cliente_id'
        DataSource = dsContareceber_Detalhe
        TabOrder = 1
      end
      object Panel2: TPanel
        Left = 0
        Top = 361
        Width = 694
        Height = 57
        Align = alBottom
        BevelOuter = bvNone
        Color = clGray
        ParentBackground = False
        TabOrder = 0
        object DBNavigator1: TDBNavigator
          AlignWithMargins = True
          Left = 3
          Top = 3
          Width = 688
          Height = 51
          DataSource = dsContareceber_Detalhe
          VisibleButtons = [nbInsert, nbDelete, nbEdit, nbPost, nbCancel, nbRefresh]
          Align = alClient
          TabOrder = 0
        end
      end
      object DBEdit3: TDBEdit
        Left = 144
        Top = 155
        Width = 121
        Height = 24
        DataField = 'contareceber_datavencimento'
        DataSource = dsContareceber_Detalhe
        TabOrder = 4
      end
      object DBEdit2: TDBEdit
        Left = 145
        Top = 97
        Width = 120
        Height = 24
        DataField = 'contareceber_datalancamento'
        DataSource = dsContareceber_Detalhe
        TabOrder = 2
      end
      object DBEdit4: TDBEdit
        Left = 144
        Top = 125
        Width = 97
        Height = 24
        DataField = 'contareceber_valor'
        DataSource = dsContareceber_Detalhe
        TabOrder = 3
      end
      object DBMemo1: TDBMemo
        Left = 144
        Top = 185
        Width = 393
        Height = 97
        DataField = 'contareceber_observacao'
        DataSource = dsContareceber_Detalhe
        TabOrder = 5
      end
    end
  end
  object dsContareceber_Pesquisa: TDataSource
    AutoEdit = False
    DataSet = DtmCRE.Contareceber_Pesquisa
    Left = 618
    Top = 69
  end
  object dsContareceber_Detalhe: TDataSource
    AutoEdit = False
    DataSet = DtmCRE.Contareceber_Detalhe
    Left = 618
    Top = 141
  end
end
