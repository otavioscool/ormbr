inherited FrmMasterDetailNormal: TFrmMasterDetailNormal
  Caption = 'FrmMasterDetailNormal'
  ClientHeight = 491
  ClientWidth = 659
  ExplicitWidth = 675
  ExplicitHeight = 530
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnTitulo: TPanel
    Width = 659
    ExplicitWidth = 659
  end
  object DBGrid_Venda: TDBGrid
    Left = 80
    Top = 31
    Width = 571
    Height = 226
    DataSource = dsMaster
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object DBGrid_vendaitem: TDBGrid
    Left = 80
    Top = 280
    Width = 329
    Height = 161
    DataSource = dsDetail
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object Button1: TButton
    Left = 80
    Top = 447
    Width = 75
    Height = 25
    Caption = 'apply'
    TabOrder = 3
    OnClick = Button1Click
  end
  object Memo1: TMemo
    Left = 424
    Top = 272
    Width = 217
    Height = 211
    Lines.Strings = (
      'Unit :'
      '       FireDac.Comp.Dataset'
      ''
      'Fun'#231'ao:'
      '          ResyncViews;'
      '          GetRow;')
    TabOrder = 4
  end
  object FDQMaster: TFDQuery
    CachedUpdates = True
    Connection = DtmCONN.FDConnection1
    SchemaAdapter = FDSchemaAdapter1
    SQL.Strings = (
      'select * from venda')
    Left = 16
    Top = 48
    object FDQMastervenda_Id: TFDAutoIncField
      FieldName = 'venda_Id'
      Origin = 'venda_Id'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object FDQMastervenda_datalancamento: TDateField
      FieldName = 'venda_datalancamento'
      Origin = 'venda_datalancamento'
      Required = True
    end
    object FDQMastercliente_id: TIntegerField
      FieldName = 'cliente_id'
      Origin = 'cliente_id'
      Required = True
    end
    object FDQMasterformapagto_id: TIntegerField
      FieldName = 'formapagto_id'
      Origin = 'formapagto_id'
    end
    object FDQMastervenda_Observacao: TStringField
      FieldName = 'venda_Observacao'
      Origin = 'venda_Observacao'
      Size = 250
    end
    object FDQMastervenda_dataalteracao: TDateField
      FieldName = 'venda_dataalteracao'
      Origin = 'venda_dataalteracao'
      Required = True
    end
  end
  object dsMaster: TDataSource
    DataSet = FDQMaster
    Left = 16
    Top = 96
  end
  object FDGUIxWaitCursor1: TFDGUIxWaitCursor
    Provider = 'Forms'
    Left = 600
    Top = 216
  end
  object FDQDetail: TFDQuery
    CachedUpdates = True
    IndexFieldNames = 'venda_id'
    MasterSource = dsMaster
    MasterFields = 'venda_Id'
    DetailFields = 'venda_id'
    Connection = DtmCONN.FDConnection1
    SchemaAdapter = FDSchemaAdapter1
    FetchOptions.AssignedValues = [evDetailCascade]
    FetchOptions.DetailCascade = True
    SQL.Strings = (
      'Select * from  vendaitem where venda_id = :venda_id')
    Left = 16
    Top = 296
    ParamData = <
      item
        Name = 'VENDA_ID'
        DataType = ftAutoInc
        ParamType = ptInput
      end>
  end
  object dsDetail: TDataSource
    DataSet = FDQDetail
    Left = 16
    Top = 352
  end
  object FDSchemaAdapter1: TFDSchemaAdapter
    Left = 16
    Top = 208
  end
end
