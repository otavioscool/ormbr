inherited FrmMovimentoVenda: TFrmMovimentoVenda
  Caption = 'Movimento de Venda'
  ClientHeight = 573
  ClientWidth = 787
  OnDestroy = FormDestroy
  ExplicitWidth = 803
  ExplicitHeight = 612
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnTitulo: TPanel
    Width = 787
    ExplicitWidth = 787
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 25
    Width = 787
    Height = 548
    ActivePage = tabVND_Detalhe
    Align = alClient
    TabOrder = 1
    object tabVND_Pesquisa: TTabSheet
      Caption = 'Pesquisa'
      object DBGrid1: TDBGrid
        Left = 0
        Top = 0
        Width = 779
        Height = 520
        Align = alClient
        DataSource = dsVenda_Pesquisa
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
      end
    end
    object tabVND_Detalhe: TTabSheet
      Caption = 'Detalhe'
      ImageIndex = 1
      OnShow = tabVND_DetalheShow
      object Panel2: TPanel
        Left = 0
        Top = 463
        Width = 779
        Height = 57
        Align = alBottom
        BevelOuter = bvNone
        Color = clGray
        ParentBackground = False
        TabOrder = 0
        object DBNavigator1: TDBNavigator
          AlignWithMargins = True
          Left = 3
          Top = 3
          Width = 773
          Height = 51
          DataSource = dsVenda
          VisibleButtons = [nbPrior, nbNext, nbInsert, nbDelete, nbEdit, nbPost, nbCancel, nbRefresh]
          Align = alClient
          TabOrder = 0
        end
      end
      object Panel1: TPanel
        Left = 0
        Top = 225
        Width = 779
        Height = 238
        Align = alClient
        TabOrder = 1
        object DBGrid2: TDBGrid
          Left = 1
          Top = 27
          Width = 777
          Height = 210
          Align = alClient
          DataSource = dsVendaItem
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = []
        end
        object Panel3: TPanel
          Left = 1
          Top = 1
          Width = 777
          Height = 26
          Align = alTop
          BevelOuter = bvNone
          Color = clSilver
          ParentBackground = False
          TabOrder = 1
          object Label6: TLabel
            AlignWithMargins = True
            Left = 3
            Top = 3
            Width = 49
            Height = 20
            Align = alLeft
            Caption = 'Produto:'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
            Layout = tlCenter
            ExplicitHeight = 16
          end
          object SpeedButton2: TSpeedButton
            Left = 113
            Top = 0
            Width = 23
            Height = 26
            Align = alLeft
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
            ExplicitLeft = 176
            ExplicitTop = 7
            ExplicitHeight = 22
          end
          object DBText3: TDBText
            AlignWithMargins = True
            Left = 139
            Top = 3
            Width = 49
            Height = 20
            Align = alLeft
            AutoSize = True
            Color = 4227200
            DataField = 'produto_descricao'
            DataSource = dsVendaItem
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Arial'
            Font.Style = []
            ParentColor = False
            ParentFont = False
            ExplicitHeight = 16
          end
          object Label7: TLabel
            Left = 556
            Top = 0
            Width = 76
            Height = 26
            Align = alRight
            Caption = 'Vr. Unitario:  '
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
            Layout = tlCenter
            ExplicitHeight = 16
          end
          object Label8: TLabel
            Left = 447
            Top = 0
            Width = 33
            Height = 26
            Align = alRight
            Caption = 'Qtd.: '
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
            Layout = tlCenter
            ExplicitHeight = 16
          end
          object edtProduto_id: TDBEdit
            Left = 55
            Top = 0
            Width = 58
            Height = 26
            Align = alLeft
            DataField = 'produto_id'
            DataSource = dsVendaItem
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            ExplicitHeight = 24
          end
          object DBEdit3: TDBEdit
            Left = 632
            Top = 0
            Width = 80
            Height = 26
            Align = alRight
            DataField = 'vendaitem_valorunitario'
            DataSource = dsVendaItem
            TabOrder = 2
            ExplicitHeight = 21
          end
          object DBEdit5: TDBEdit
            Left = 480
            Top = 0
            Width = 76
            Height = 26
            Align = alRight
            DataField = 'vendaitem_quantidade'
            DataSource = dsVendaItem
            TabOrder = 1
            ExplicitHeight = 21
          end
          object DBNavigator2: TDBNavigator
            Left = 712
            Top = 0
            Width = 65
            Height = 26
            DataSource = dsVendaItem
            VisibleButtons = [nbInsert, nbPost]
            Align = alRight
            TabOrder = 3
          end
        end
      end
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 779
        Height = 225
        Align = alTop
        TabOrder = 2
        object DBText1: TDBText
          Left = 144
          Top = 14
          Width = 65
          Height = 17
          DataField = 'venda_Id'
          DataSource = dsVenda
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label1: TLabel
          Left = 89
          Top = 14
          Width = 49
          Height = 16
          Caption = 'Codigo:'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label2: TLabel
          Left = 32
          Top = 41
          Width = 106
          Height = 16
          Caption = 'Data Lan'#231'amento:'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object Label3: TLabel
          Left = 66
          Top = 131
          Width = 72
          Height = 16
          Caption = 'Observa'#231#227'o:'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object Label4: TLabel
          Left = 94
          Top = 73
          Width = 44
          Height = 16
          Caption = 'Cliente:'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object SpeedButton1: TSpeedButton
          Left = 255
          Top = 71
          Width = 23
          Height = 22
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object DBText2: TDBText
          Left = 284
          Top = 80
          Width = 49
          Height = 16
          AutoSize = True
          DataField = 'cliente_nome'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object Label5: TLabel
          Left = 27
          Top = 105
          Width = 111
          Height = 16
          Caption = 'Forma Pagamento:'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object DBEdit1: TDBEdit
          Left = 144
          Top = 38
          Width = 108
          Height = 24
          DataField = 'venda_datalancamento'
          DataSource = dsVenda
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
        end
        object DBEdit4: TDBEdit
          Left = 144
          Top = 70
          Width = 108
          Height = 24
          DataField = 'cliente_id'
          DataSource = dsVenda
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
        end
        object DBLookupComboBox1: TDBLookupComboBox
          Left = 144
          Top = 102
          Width = 196
          Height = 24
          DataField = 'formapagto_id'
          DataSource = dsVenda
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          KeyField = 'formapagto_id'
          ListField = 'formapagto_descricao'
          ListSource = dsLookup_Formapagamento
          ParentFont = False
          TabOrder = 2
        end
        object DBMemo1: TDBMemo
          Left = 144
          Top = 132
          Width = 313
          Height = 64
          DataField = 'venda_observacao'
          DataSource = dsVenda
          TabOrder = 3
        end
      end
    end
  end
  object dsVenda_Pesquisa: TDataSource
    DataSet = DtmMVD.Venda_Pesquisa
    Left = 536
    Top = 64
  end
  object dsLookFind_cliente: TDataSource
    DataSet = DtmMVD.LookFind_Cliente
    Left = 688
    Top = 64
  end
  object dsLookup_Formapagamento: TDataSource
    DataSet = DtmMVD.Lookup_Formapagamento
    Left = 512
    Top = 416
  end
  object dsLookup_produto: TDataSource
    AutoEdit = False
    DataSet = DtmMVD.Lookup_Produto
    Left = 648
    Top = 408
  end
  object dsVenda: TDataSource
    AutoEdit = False
    DataSet = DtmMVD.Venda
    Left = 32
    Top = 216
  end
  object dsVendaItem: TDataSource
    AutoEdit = False
    DataSet = DtmMVD.Venda_Item
    Left = 40
    Top = 376
  end
end
