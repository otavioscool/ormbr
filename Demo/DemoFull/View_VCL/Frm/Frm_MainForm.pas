unit Frm_MainForm;

interface

uses
  Windows,
  Messages,
  SysUtils,
  Variants,
  Classes,
  Graphics,
  Controls,
  Forms,
  Dialogs,
  DB,
  Grids,
  DBGrids,
  StdCtrls,
  Mask,
  DBClient,
  DBCtrls,
  ExtCtrls,
  /// orm factory
  ormbr.factory.interfaces,
  /// orm injection dependency
  ormbr.dependency.interfaces,
  ormbr.factory.firedac,
  /// orm model
  demo.model.venda,
  demo.model.vendaitem,
  demo.model.cliente,

  FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys,
  FireDAC.VCLUI.Wait, FireDAC.Comp.Client, FireDAC.Stan.Intf,
  FireDAC.Phys.SQLite, FireDAC.Phys.SQLiteDef, FireDAC.Stan.ExprFuncs,
  FireDAC.Comp.UI, FireDAC.DApt, Vcl.ComCtrls, Vcl.Imaging.pngimage, Vcl.Buttons,
  Vcl.CategoryButtons, System.Actions, Vcl.ActnList, Frm_CadastroCliente,
  Frm_CadastroProduto, Frm_MovimentoVenda, Frm_CadastroFormaPagamento;


type
  TFrmMainForm = class(TForm)
    Panel1: TPanel;
    Image1: TImage;
    Label8: TLabel;
    CategoryButtons1: TCategoryButtons;
    ActionList: TActionList;
    ACT_CadastroCliente: TAction;
    ACT_CadastroProduto: TAction;
    ACT_Vendas: TAction;
    ACT_CadastroFormaPagamento: TAction;
    ACT_ContasReceber: TAction;
    procedure ACT_CadastroClienteExecute(Sender: TObject);
    procedure ACT_CadastroProdutoExecute(Sender: TObject);
    procedure ACT_VendasExecute(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure ACT_CadastroFormaPagamentoExecute(Sender: TObject);
    procedure ACT_ContasReceberExecute(Sender: TObject);
  private
    { Private declarations }
    procedure OpenForm(FClass: TFormClass; var Instance);
  public
    { Public declarations }
  end;

var
  FrmMainForm: TFrmMainForm;

implementation

uses
  StrUtils, Dtm_Connection, Frm_ContaReceber;

{$R *.dfm}


procedure TFrmMainForm.OpenForm(FClass: TFormClass; var Instance);
var
  I: Integer;
begin
   for I := 0 to MDIChildCount - 1 do
   begin
     MDIChildren[I].Free;
   end;
   Application.CreateForm(FClass, Instance);
end;


procedure TFrmMainForm.ACT_CadastroClienteExecute(Sender: TObject);
begin
  OpenForm(TFrmCadastroCliente,FrmCadastroCliente);
end;

procedure TFrmMainForm.ACT_CadastroFormaPagamentoExecute(Sender: TObject);
begin
  OpenForm(TFrmCadastroFormaPagamento,FrmCadastroFormaPagamento);
end;

procedure TFrmMainForm.ACT_CadastroProdutoExecute(Sender: TObject);
begin
  OpenForm(TFrmCadastroProduto,FrmCadastroProduto);
end;

procedure TFrmMainForm.ACT_ContasReceberExecute(Sender: TObject);
begin
  OpenForm(TFrmContaReceber,FrmContaReceber);
end;

procedure TFrmMainForm.ACT_VendasExecute(Sender: TObject);
begin
  OpenForm(TFrmMovimentoVenda,FrmMovimentoVenda);
end;

procedure TFrmMainForm.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
var
  I: Integer;
begin
   for I := 0 to Self.MDIChildCount - 1 do
   begin
      Self.MDIChildren[I].Free;
   end;
end;

end.
