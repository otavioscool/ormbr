inherited FrmCadastroCliente: TFrmCadastroCliente
  BorderStyle = bsNone
  Caption = 'Cadastro de Cliente'
  ClientHeight = 500
  ClientWidth = 789
  OnDestroy = FormDestroy
  ExplicitWidth = 805
  ExplicitHeight = 539
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnTitulo: TPanel
    Width = 789
    TabOrder = 1
    ExplicitWidth = 789
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 25
    Width = 789
    Height = 475
    ActivePage = tabCLI_Pesquisa
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object tabCLI_Pesquisa: TTabSheet
      Caption = 'Pesquisa'
      object DBGrid1: TDBGrid
        Left = 0
        Top = 0
        Width = 781
        Height = 444
        Align = alClient
        DataSource = dsCliente_Pesquisa
        TabOrder = 0
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -13
        TitleFont.Name = 'Arial'
        TitleFont.Style = []
      end
    end
    object tabCLI_Detalhe: TTabSheet
      Caption = 'Detalhe'
      ImageIndex = 1
      OnShow = tabCLI_DetalheShow
      object DBText1: TDBText
        Left = 108
        Top = 16
        Width = 65
        Height = 17
        DataField = 'cliente_id'
        DataSource = dsCliente_Detalhe
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label1: TLabel
        Left = 53
        Top = 15
        Width = 49
        Height = 16
        Caption = 'Codigo:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label2: TLabel
        Left = 64
        Top = 42
        Width = 38
        Height = 16
        Caption = 'Nome:'
      end
      object Label3: TLabel
        Left = 43
        Top = 72
        Width = 59
        Height = 16
        Caption = 'Endereco:'
      end
      object Label4: TLabel
        Left = 58
        Top = 102
        Width = 44
        Height = 16
        Caption = 'Cidade:'
      end
      object Label5: TLabel
        Left = 51
        Top = 132
        Width = 51
        Height = 16
        Caption = 'Telefone:'
      end
      object Label6: TLabel
        Left = 30
        Top = 161
        Width = 72
        Height = 16
        Caption = 'Observa'#231#227'o:'
      end
      object DBEdit1: TDBEdit
        Left = 108
        Top = 39
        Width = 441
        Height = 24
        DataField = 'cliente_nome'
        DataSource = dsCliente_Detalhe
        TabOrder = 0
      end
      object Panel2: TPanel
        Left = 0
        Top = 387
        Width = 781
        Height = 57
        Align = alBottom
        BevelOuter = bvNone
        Color = clGray
        ParentBackground = False
        TabOrder = 5
        object DBNavigator1: TDBNavigator
          AlignWithMargins = True
          Left = 3
          Top = 3
          Width = 775
          Height = 51
          DataSource = dsCliente_Detalhe
          VisibleButtons = [nbInsert, nbDelete, nbEdit, nbPost, nbCancel, nbRefresh]
          Align = alClient
          TabOrder = 0
        end
      end
      object DBEdit2: TDBEdit
        Left = 108
        Top = 69
        Width = 441
        Height = 24
        DataField = 'cliente_endereco'
        DataSource = dsCliente_Detalhe
        TabOrder = 1
      end
      object DBEdit3: TDBEdit
        Left = 108
        Top = 99
        Width = 365
        Height = 24
        DataField = 'cliente_cidade'
        DataSource = dsCliente_Detalhe
        TabOrder = 2
      end
      object DBEdit4: TDBEdit
        Left = 108
        Top = 129
        Width = 229
        Height = 24
        DataField = 'cliente_telefone'
        DataSource = dsCliente_Detalhe
        TabOrder = 3
      end
      object DBMemo1: TDBMemo
        Left = 108
        Top = 159
        Width = 441
        Height = 126
        DataField = 'cliente_observacao'
        DataSource = dsCliente_Detalhe
        TabOrder = 4
      end
    end
  end
  object dsCliente_Pesquisa: TDataSource
    AutoEdit = False
    DataSet = DtmCLI.Cliente_Pesquisa
    Left = 727
    Top = 61
  end
  object dsCliente_Detalhe: TDataSource
    AutoEdit = False
    DataSet = DtmCLI.Cliente_Detalhe
    Left = 727
    Top = 133
  end
end
