unit Frm_ContaReceber;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Frm_MDIChildInherit,
  Vcl.ExtCtrls, Data.DB, Vcl.DBCtrls, Vcl.StdCtrls, Vcl.Mask, Vcl.Grids,
  Vcl.DBGrids, Vcl.ComCtrls;

type
  TFrmContaReceber = class(TFrmMDIChildInherit)
    PageControl1: TPageControl;
    tabCLI_Pesquisa: TTabSheet;
    DBGrid1: TDBGrid;
    tabCLI_Detalhe: TTabSheet;
    DBText1: TDBText;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    Panel2: TPanel;
    DBNavigator1: TDBNavigator;
    DBEdit3: TDBEdit;
    dsContareceber_Pesquisa: TDataSource;
    dsContareceber_Detalhe: TDataSource;
    DBEdit2: TDBEdit;
    DBEdit4: TDBEdit;
    Label4: TLabel;
    Label5: TLabel;
    DBMemo1: TDBMemo;
    Label6: TLabel;
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure tabCLI_DetalheShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmContaReceber: TFrmContaReceber;

implementation

{$R *.dfm}

uses Dtm_ContaReceber;

procedure TFrmContaReceber.FormCreate(Sender: TObject);
begin
  inherited;
  Application.CreateForm(TDtmCRE,DtmCRE);
  tabCLI_Pesquisa.Show;
end;

procedure TFrmContaReceber.FormDestroy(Sender: TObject);
begin
   FreeAndNil(DtmCRE);
   inherited;
end;

procedure TFrmContaReceber.tabCLI_DetalheShow(Sender: TObject);
begin
  inherited;
   DtmCRE.Open_Detalhe(DtmCRE.Contareceber_Pesquisa.FieldByName('contareceber_id').AsInteger);
end;

end.
