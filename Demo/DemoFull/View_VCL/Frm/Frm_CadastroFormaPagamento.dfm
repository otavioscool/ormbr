inherited FrmCadastroFormaPagamento: TFrmCadastroFormaPagamento
  Caption = 'Cadastro de Forma Pagamento'
  ClientHeight = 474
  ClientWidth = 702
  OnDestroy = FormDestroy
  ExplicitWidth = 718
  ExplicitHeight = 513
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnTitulo: TPanel
    Width = 702
    ExplicitWidth = 702
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 25
    Width = 702
    Height = 449
    ActivePage = tabFPG_Detalhe
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object tabFPG_Pesquisa: TTabSheet
      Caption = 'Pesquisa'
      object DBGrid1: TDBGrid
        Left = 0
        Top = 0
        Width = 694
        Height = 418
        Align = alClient
        DataSource = dsFormaPagto_Pesquisa
        TabOrder = 0
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -13
        TitleFont.Name = 'Arial'
        TitleFont.Style = []
      end
    end
    object tabFPG_Detalhe: TTabSheet
      Caption = 'Detalhe'
      ImageIndex = 1
      OnShow = tabFPG_DetalheShow
      object DBText1: TDBText
        Left = 144
        Top = 48
        Width = 65
        Height = 17
        DataField = 'formapagto_id'
        DataSource = dsFormaPagto_Detalhe
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label1: TLabel
        Left = 88
        Top = 48
        Width = 49
        Height = 16
        Caption = 'Codigo:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label3: TLabel
        Left = 74
        Top = 74
        Width = 62
        Height = 16
        Caption = 'Descri'#231#227'o:'
      end
      object Panel2: TPanel
        Left = 0
        Top = 361
        Width = 694
        Height = 57
        Align = alBottom
        BevelOuter = bvNone
        Color = clGray
        ParentBackground = False
        TabOrder = 0
        object DBNavigator1: TDBNavigator
          AlignWithMargins = True
          Left = 3
          Top = 3
          Width = 688
          Height = 51
          DataSource = dsFormaPagto_Detalhe
          VisibleButtons = [nbInsert, nbDelete, nbEdit, nbPost, nbCancel, nbRefresh]
          Align = alClient
          TabOrder = 0
        end
      end
      object DBEdit2: TDBEdit
        Left = 144
        Top = 71
        Width = 312
        Height = 24
        DataField = 'formapagto_descricao'
        DataSource = dsFormaPagto_Detalhe
        TabOrder = 1
      end
    end
  end
  object dsFormaPagto_Pesquisa: TDataSource
    AutoEdit = False
    DataSet = DtmFPG.FormaPagto_Pesquisa
    Left = 618
    Top = 69
  end
  object dsFormaPagto_Detalhe: TDataSource
    AutoEdit = False
    DataSet = DtmFPG.FormaPagto_Detalhe
    Left = 618
    Top = 141
  end
end
