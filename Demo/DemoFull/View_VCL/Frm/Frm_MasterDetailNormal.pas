unit Frm_MasterDetailNormal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Frm_MDIChildInherit, Vcl.ExtCtrls,
  Dtm_Connection, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, Datasnap.DBClient, Datasnap.Provider, Vcl.Grids,
  Vcl.DBGrids, FireDAC.UI.Intf, FireDAC.VCLUI.Wait, FireDAC.Comp.UI,
  Vcl.StdCtrls;

type
  TFrmMasterDetailNormal = class(TFrmMDIChildInherit)
    FDQMaster: TFDQuery;
    dsMaster: TDataSource;
    DBGrid_Venda: TDBGrid;
    DBGrid_vendaitem: TDBGrid;
    FDGUIxWaitCursor1: TFDGUIxWaitCursor;
    FDQMastervenda_Id: TFDAutoIncField;
    FDQMastervenda_datalancamento: TDateField;
    FDQMastercliente_id: TIntegerField;
    FDQMasterformapagto_id: TIntegerField;
    FDQMastervenda_Observacao: TStringField;
    FDQMastervenda_dataalteracao: TDateField;
    FDQDetail: TFDQuery;
    dsDetail: TDataSource;
    Button1: TButton;
    FDSchemaAdapter1: TFDSchemaAdapter;
    Memo1: TMemo;
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmMasterDetailNormal: TFrmMasterDetailNormal;

implementation

{$R *.dfm}

procedure TFrmMasterDetailNormal.Button1Click(Sender: TObject);
begin
  inherited;
   FDSchemaAdapter1.ApplyUpdates(0);
end;

procedure TFrmMasterDetailNormal.FormCreate(Sender: TObject);
begin
  inherited;
   FDQMaster.Open;
   FDQDetail.Open;
end;

end.
