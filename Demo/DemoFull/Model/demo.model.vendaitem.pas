unit demo.model.vendaitem;

interface

uses
  Classes, 
  DB, 
  SysUtils, 
  Generics.Collections, 
  /// orm 
  ormbr.mapping.attributes, 
  ormbr.mapping.classes,
  ormbr.types.nullable,
  ormbr.types.lazy,
  ormbr.types.mapping,
  demo.model.produto;

type
  [Entity]
  [Table('vendaitem','')]
  [PrimaryKey('vendaitem_id', AutoInc)]
  [Sequence('SEQ_VENDAITEM')]
  Tvendaitem = class
  private
    { Private declarations }
    Fvendaitem_id: Integer;
    Fvenda_id: Integer;
    Fproduto_id: Integer;
    Fproduto_descricao: String;
    Fvendaitem_quantidade: Integer;
    Fvendaitem_valorunitario: Currency;
    Fvendaitem_valortotal: Currency;
    Fproduto: Tproduto;
public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;

    [Column('vendaitem_id', ftInteger)]
    [Dictionary('vendaitem_id','Mensagem de valida��o','','','',taCenter)]
    property vendaitem_id: Integer Index 0 read Fvendaitem_id write Fvendaitem_id;

    [Column('venda_id', ftInteger)]
    [Dictionary('venda_id','Mensagem de valida��o','','','',taCenter)]
    property venda_id: Integer Index 1 read Fvenda_id write Fvenda_id;

    [Column('produto_id', ftInteger)]
    [Dictionary('produto_id','Mensagem de valida��o','','','',taCenter)]
    property produto_id: Integer Index 2 read Fproduto_id write Fproduto_id;

    [Column('vendaitem_quantidade', ftInteger)]
    [Dictionary('vendaitem_quantidade','Mensagem de valida��o','0','','',taRightJustify)]
    property vendaitem_quantidade: Integer Index 3 read Fvendaitem_quantidade write Fvendaitem_quantidade;

    [Column('vendaitem_valorunitario', ftCurrency)]
    [Dictionary('vendaitem_valorunitario','Mensagem de valida��o','0','','',taRightJustify)]
    property vendaitem_valorunitario: Currency Index 4 read Fvendaitem_valorunitario write Fvendaitem_valorunitario;

    [Column('vendaitem_valortotal', ftCurrency)]
    [Dictionary('vendaitem_valortotal','Mensagem de valida��o','0','','',taRightJustify)]
    property vendaitem_valortotal: Currency Index 5 read Fvendaitem_valortotal write Fvendaitem_valortotal;

    [ForeignKey('produto', None, None)]
    property produto: Tproduto read Fproduto write Fproduto;

  end;

implementation

{ Tdetail }

constructor Tvendaitem.Create;
begin
  inherited;
  Fproduto := Tproduto.Create;
end;

destructor Tvendaitem.Destroy;
begin
  Fproduto.Free;
  inherited;
end;

end.
