unit demo.model.produto;

interface

uses
  Classes, 
  DB, 
  SysUtils, 
  Generics.Collections, 
  /// orm 
  ormbr.mapping.attributes, 
  ormbr.mapping.classes,
  ormbr.types.nullable,
  ormbr.types.mapping,
  ormbr.types.lazy;

type
  [Entity]
  [Table('produto','')]
  [PrimaryKey('produto_id', AutoInc)]
  [Sequence('SEQ_PRODUTO')]
  Tproduto = class
  private
    { Private declarations }
    Fproduto_id: Integer;
    Fproduto_unidade: String;
    Fproduto_descricao: String;
    Fproduto_embalagem: String;
    Fproduto_preco: Currency;
  public
    { Public declarations }
    [Column('produto_id', ftInteger)]
    [Dictionary('produto_id','Mensagem de valida��o','','','',taCenter)]
    property produto_id: Integer Index 0 read Fproduto_id write Fproduto_id;

    [Column('produto_unidade', ftString)]
    [Dictionary('produto_unidade','Mensagem de valida��o','','','',taLeftJustify)]
    property produto_unidade: String Index 1 read Fproduto_unidade write Fproduto_unidade;

    [Column('produto_descricao', ftString)]
    [Indexe('idx_produto_descricao','produto_descricao')]
    [Dictionary('produto_descricao','Mensagem de valida��o','','','',taLeftJustify)]
    property produto_descricao: String Index 2 read Fproduto_descricao write Fproduto_descricao;

    [Column('produto_embalagem', ftString)]
    [Dictionary('Embalagem','Mensagem de valida��o','','','',taLeftJustify)]
    property produto_embalagem: String Index 3 read Fproduto_embalagem write Fproduto_embalagem;

    [Column('produto_preco', ftCurrency)]
    [Dictionary('produto_preco','Mensagem de valida��o','','','',taLeftJustify)]
    property produto_preco: Currency Index 4 read Fproduto_preco write Fproduto_preco;

  end;

implementation

end.
