unit demo.model.contareceber;

interface

uses
  Classes, 
  DB, 
  SysUtils, 
  Generics.Collections, 
  /// orm 
  ormbr.mapping.attributes, 
  ormbr.mapping.classes,
  ormbr.types.nullable,
  ormbr.types.lazy,
  ormbr.types.mapping,
  demo.model.cliente;

type
  [Entity]
  [Table('contareceber','')]
  [PrimaryKey('contareceber_id', AutoInc)]
  [Sequence('SEQ_CONTASRECEBER')]
  Tcontareceber = class
  private
    { Private declarations }
    Fcontareceber_id : integer;
    Fcliente_id : integer;
    Fcontareceber_documento : integer;
    Fcontareceber_datalancamento : TDateTime;
    Fcontareceber_valor : Currency;
    Fcontareceber_datavencimento : TDateTime;
    Fcontareceber_observacao : string;
    Fcliente: Tcliente;
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;
    [Column('contareceber_id', ftInteger)]
    [Dictionary('contareceber_id','Mensagem de valida��o','','','',taCenter)]
    property contareceber_id: Integer Index 0 read Fcontareceber_id write Fcontareceber_id;

    [Column('cliente_id', ftInteger)]
    [Dictionary('cliente_id','Mensagem de valida��o','','','',taCenter)]
    property cliente_id: Integer Index 1 read Fcliente_id write Fcliente_id;

    [Column('contareceber_documento', ftInteger)]
    [Dictionary('contareceber_documento','Mensagem de valida��o','','','',taCenter)]
    property contareceber_documento: Integer Index 2 read Fcontareceber_documento write Fcontareceber_documento;

    [Column('contareceber_datalancamento', ftDateTime)]
    [Dictionary('contareceber_datalancamento','Mensagem de valida��o','','DD/MM/YYYY','',taCenter)]
    property contareceber_datalancamento: TDateTime Index 3 read Fcontareceber_datalancamento write Fcontareceber_datalancamento;

    [Column('contareceber_valor', ftCurrency)]
    [Dictionary('contareceber_valor','Mensagem de valida��o','0','','',taRightJustify)]
    property contareceber_valor: Currency Index 4 read Fcontareceber_valor write Fcontareceber_valor;

    [Column('contareceber_datavencimento', ftDateTime)]
    [Dictionary('contareceber_datavencimento','Mensagem de valida��o',taCenter)]
    property contareceber_datavencimento: TDateTime Index 5 read Fcontareceber_datavencimento write Fcontareceber_datavencimento;

    [Column('contareceber_observacao', ftString)]
    [Dictionary('contareceber_observacao','Mensagem de valida��o','','','',taLeftJustify)]
    property contareceber_observacao: string Index 6 read Fcontareceber_observacao write Fcontareceber_observacao;

    [ForeignKey('cliente', None, None)]
    [Association(OneToOne,'cliente_id','cliente_id')]
    property cliente: Tcliente read Fcliente write Fcliente;

  end;

implementation

{ Tdetail }

constructor Tcontareceber.Create;
begin
  inherited;
  Fcliente := Tcliente.Create;
end;

destructor Tcontareceber.Destroy;
begin
  Fcliente.Free;
  inherited;
end;

end.
