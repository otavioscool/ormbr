unit demo.controller.contareceber;

interface

uses
  ormbr.bind.base,
  demo.controller.base,
  ormbr.factory.interfaces, ormbr.dependency.interfaces, System.SysUtils, dialogs;

type
  TControllerContareceber<M: class, constructor> = class(TControllerBaseDemo<M>)
  public
    constructor Create(AConnection: IDBConnection; ADataSet: TDatasetBase<M>); override;
    destructor Destroy; override;
  end;

implementation


{ TControllerCliente<M> }

uses demo.model.contareceber, ormbr.dependency.injection.clientdataset,
  Dtm_Connection;

constructor TControllerContareceber<M>.Create(AConnection: IDBConnection;
  ADataSet: TDatasetBase<M>);
begin
  inherited;
   //
end;

destructor TControllerContareceber<M>.Destroy;
begin
  //
  inherited;
end;



end.
