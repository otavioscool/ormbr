unit demo.controller.produto;

interface

uses
  ormbr.bind.base,
  demo.controller.base,
  ormbr.factory.interfaces;

type
  TControllerProduto<M: class, constructor> = class(TControllerBaseDemo<M>)
  public
    constructor Create(AConnection: IDBConnection; ADataSet: TDatasetBase<M>); override;
    destructor Destroy; override;

  end;

implementation


{ TControllerCliente<M> }

constructor TControllerProduto<M>.Create(AConnection: IDBConnection;
  ADataSet: TDatasetBase<M>);
begin
  inherited;
   //
end;

destructor TControllerProduto<M>.Destroy;
begin
  //
  inherited;
end;

end.
