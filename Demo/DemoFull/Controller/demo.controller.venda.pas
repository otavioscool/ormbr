unit demo.controller.venda;

interface

uses
  System.SysUtils,
  ormbr.bind.base,
  demo.controller.base,
  ormbr.factory.interfaces,
  demo.model.contareceber;

type
  TControllerVenda<M: class, constructor> = class(TControllerBaseDemo<M>)
  private
  public
    constructor Create(AConnection: IDBConnection; ADataSet: TDatasetBase<M>); override;
    destructor Destroy; override;

    procedure Gera_ContaReceberVenda(pVenda_id:Integer);

  end;

implementation


{ TControllerCliente<M> }

constructor TControllerVenda<M>.Create(AConnection: IDBConnection;
  ADataSet: TDatasetBase<M>);
begin
  inherited;
   //
end;

destructor TControllerVenda<M>.Destroy;
begin
  //
  inherited;
end;


procedure TControllerVenda<M>.Gera_ContaReceberVenda(pVenda_id: Integer);
var
  oVenda: IDBResultSet;
  oContaReceber : Tcontareceber;
  iSQL : String;
begin
  inherited;
    try
        // iSQL := ICriteria.SQL.All.From('venda').Where('venda_id = '+IntToStr(pVenda_id)).AsString;
        iSQL := 'Select * from venda Where venda_id = '+IntToStr(pVenda_id);
      oVenda := FConnection.ExecuteSQL(iSQL);

      if oVenda.NotEof then
      begin
        oContaReceber := Tcontareceber.Create;
        with oContaReceber do
        begin
           contareceber_id := Self.GetSequencia('contareceber','contareceber_id');
           cliente_id := oVenda.GetFieldValue('cliente_id');
           contareceber_documento := oVenda.GetFieldValue('venda_id');
           contareceber_datalancamento := Date;
           contareceber_valor := 10;
           contareceber_datavencimento := Date;
           contareceber_observacao := 'TESTES NOVA MUDAN�A';
        end;
      end;

//      FConnection.StartTransaction;
//      FDataSetBase.Session.Insert(oContaReceber);
//      FConnection.Commit;

    finally
      oContaReceber.Free;
    end;
end;

end.
