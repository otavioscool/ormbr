unit demo.controller.base;

interface

uses
  ormbr.bind.base,
  ormbr.controller.base,
  ormbr.factory.interfaces;

type
  TControllerBaseDemo<M: class, constructor> = class(TControllerBase<M>)
  private

  public
    constructor Create(AConnection: IDBConnection; ADataSet: TDatasetBase<M>); override;
    destructor Destroy; override;

    function GetSequencia(pTable,pField:String): Integer;

  end;

implementation


{ TControllerBaseDemo }

constructor TControllerBaseDemo<M>.Create(AConnection: IDBConnection; ADataSet: TDatasetBase<M>);
begin
  inherited;

end;

destructor TControllerBaseDemo<M>.Destroy;
begin
  inherited;
end;


function TControllerBaseDemo<M>.GetSequencia(pTable, pField: String): Integer;
var
  oResultSet: IDBResultSet;
begin
  inherited;
  oResultSet := FConnection.ExecuteSQL('SELECT MAX('+pField+') AS '+pField+' FROM '+ pTable);
 try
   if oResultSet.NotEof then
     Exit(oResultSet.GetFieldValue(pField) + 1)
   else
     Exit(1);
  except
    Exit(1);
  end;
end;

end.
