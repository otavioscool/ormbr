unit demo.controller.cliente;

interface

uses
  ormbr.bind.base,
  demo.controller.base,
  ormbr.factory.interfaces;

type
  TControllerCliente<M: class, constructor> = class(TControllerBaseDemo<M>)
  public
    constructor Create(AConnection: IDBConnection; ADataSet: TDatasetBase<M>); override;
    destructor Destroy; override;

  end;

implementation


{ TControllerCliente<M> }

constructor TControllerCliente<M>.Create(AConnection: IDBConnection;
  ADataSet: TDatasetBase<M>);
begin
  inherited;
   //
end;

destructor TControllerCliente<M>.Destroy;
begin
  //
  inherited;
end;

end.
