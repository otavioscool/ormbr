program ORMBrMetadata;

uses
  System.StartUpCopy,
  FMX.Forms,
  uPrincipal in 'uPrincipal.pas' {Form4},
  ormbr.driver.connection in '..\..\..\..\Source\Drivers\ormbr.driver.connection.pas',
  ormbr.factory.connection in '..\..\..\..\Source\Drivers\ormbr.factory.connection.pas',
  ormbr.factory.interfaces in '..\..\..\..\Source\Drivers\ormbr.factory.interfaces.pas',
  ormbr.model.person in '..\..\Modelos\ormbr.model.person.pas',
  ormbr.mapping.attributes in '..\..\..\..\Source\Core\ormbr.mapping.attributes.pas',
  ormbr.mapping.register in '..\..\..\..\Source\Core\ormbr.mapping.register.pas',
  ormbr.types.mapping in '..\..\..\..\Source\Core\ormbr.types.mapping.pas',
  ormbr.types.database in '..\..\..\..\Source\Core\ormbr.types.database.pas',
  ormbr.mapping.exceptions in '..\..\..\..\Source\Core\ormbr.mapping.exceptions.pas',
  ormbr.mapping.rttiutils in '..\..\..\..\Source\Core\ormbr.mapping.rttiutils.pas',
  ormbr.mapping.repository in '..\..\..\..\Source\Core\ormbr.mapping.repository.pas',
  ormbr.mapping.classes in '..\..\..\..\Source\Core\ormbr.mapping.classes.pas',
  ormbr.mapping.explorer in '..\..\..\..\Source\Core\ormbr.mapping.explorer.pas',
  ormbr.mapping.explorerstrategy in '..\..\..\..\Source\Core\ormbr.mapping.explorerstrategy.pas',
  ormbr.mapping.popular in '..\..\..\..\Source\Core\ormbr.mapping.popular.pas',
  ormbr.types.nullable in '..\..\..\..\Source\Core\ormbr.types.nullable.pas',
  ormbr.driver.firedac in '..\..\..\..\Source\Drivers\ormbr.driver.firedac.pas',
  ormbr.driver.firedac.transaction in '..\..\..\..\Source\Drivers\ormbr.driver.firedac.transaction.pas',
  ormbr.factory.firedac in '..\..\..\..\Source\Drivers\ormbr.factory.firedac.pas',
  ormbr.objects.helper in '..\..\..\..\Source\Core\ormbr.objects.helper.pas',
  ormbr.rtti.helper in '..\..\..\..\Source\Core\ormbr.rtti.helper.pas',
  ormbr.sql.commands in '..\..\..\..\Source\Core\ormbr.sql.commands.pas',
  ormbr.database.abstract in '..\..\..\..\Source\Metadata\ormbr.database.abstract.pas',
  ormbr.database.manager in '..\..\..\..\Source\Metadata\ormbr.database.manager.pas',
  ormbr.database.mapping in '..\..\..\..\Source\Metadata\ormbr.database.mapping.pas',
  ormbr.ddl.commands in '..\..\..\..\Source\Metadata\ormbr.ddl.commands.pas',
  ormbr.ddl.generator.firebird in '..\..\..\..\Source\Metadata\ormbr.ddl.generator.firebird.pas',
  ormbr.ddl.generator.interbase in '..\..\..\..\Source\Metadata\ormbr.ddl.generator.interbase.pas',
  ormbr.ddl.generator.mssql in '..\..\..\..\Source\Metadata\ormbr.ddl.generator.mssql.pas',
  ormbr.ddl.generator.mysql in '..\..\..\..\Source\Metadata\ormbr.ddl.generator.mysql.pas',
  ormbr.ddl.generator in '..\..\..\..\Source\Metadata\ormbr.ddl.generator.pas',
  ormbr.ddl.generator.postgresql in '..\..\..\..\Source\Metadata\ormbr.ddl.generator.postgresql.pas',
  ormbr.ddl.generator.sqlite in '..\..\..\..\Source\Metadata\ormbr.ddl.generator.sqlite.pas',
  ormbr.ddl.interfaces in '..\..\..\..\Source\Metadata\ormbr.ddl.interfaces.pas',
  ormbr.ddl.register in '..\..\..\..\Source\Metadata\ormbr.ddl.register.pas',
  ormbr.metadata.classe.factory in '..\..\..\..\Source\Metadata\ormbr.metadata.classe.factory.pas',
  ormbr.metadata.db.factory in '..\..\..\..\Source\Metadata\ormbr.metadata.db.factory.pas',
  ormbr.metadata.extract in '..\..\..\..\Source\Metadata\ormbr.metadata.extract.pas',
  ormbr.metadata.firebird in '..\..\..\..\Source\Metadata\ormbr.metadata.firebird.pas',
  ormbr.metadata.interbase in '..\..\..\..\Source\Metadata\ormbr.metadata.interbase.pas',
  ormbr.metadata.interfaces in '..\..\..\..\Source\Metadata\ormbr.metadata.interfaces.pas',
  ormbr.metadata.model in '..\..\..\..\Source\Metadata\ormbr.metadata.model.pas',
  ormbr.metadata.mssql in '..\..\..\..\Source\Metadata\ormbr.metadata.mssql.pas',
  ormbr.metadata.mysql in '..\..\..\..\Source\Metadata\ormbr.metadata.mysql.pas',
  ormbr.metadata.postgresql in '..\..\..\..\Source\Metadata\ormbr.metadata.postgresql.pas',
  ormbr.metadata.register in '..\..\..\..\Source\Metadata\ormbr.metadata.register.pas',
  ormbr.metadata.sqlite in '..\..\..\..\Source\Metadata\ormbr.metadata.sqlite.pas',
  ormbr.types.fields in '..\..\..\..\Source\Metadata\ormbr.types.fields.pas';

{$R *.res}

begin
  Application.Initialize;
  ReportMemoryLeaksOnShutdown := DebugHook <> 0;
  Application.CreateForm(TForm4, Form4);
  Application.Run;
end.
