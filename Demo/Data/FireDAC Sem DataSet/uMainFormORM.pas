unit uMainFormORM;

interface

uses
  Windows,
  Messages,
  SysUtils,
  Variants,
  Classes,
  Graphics,
  Controls,
  Forms,
  Dialogs,
  DB,
  Grids,
  DBGrids,
  StdCtrls,
  Mask,
  DBClient,
  DBCtrls,
  ExtCtrls,
  /// orm factory
  ormbr.factory.interfaces,
  /// orm injection dependency
  ormbr.criteria,
  ormbr.factory.firedac,
  ormbr.types.database,
  ormbr.session.manager,
  /// orm model
  ormbr.model.master,
  ormbr.model.detail,
  ormbr.model.lookup,
  ormbr.model.client,

  FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys,
  FireDAC.VCLUI.Wait, FireDAC.Comp.Client, FireDAC.Stan.Intf,
  FireDAC.Phys.SQLite, FireDAC.Phys.SQLiteDef, FireDAC.Stan.ExprFuncs,
  FireDAC.Comp.UI, FireDAC.DApt, FireDAC.Stan.Param, FireDAC.DatS,
  FireDAC.DApt.Intf, FireDAC.Comp.DataSet;

type
  TStringGridHack = class(TStringGrid)
  protected
    procedure DeleteRow(ARow: Longint); reintroduce;
    procedure InsertRow(ARow: Longint);
  end;

  TForm3 = class(TForm)
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Label7: TLabel;
    FDConnection1: TFDConnection;
    FDPhysSQLiteDriverLink1: TFDPhysSQLiteDriverLink;
    FDGUIxWaitCursor1: TFDGUIxWaitCursor;
    StringGridMaster: TStringGrid;
    StringGridDetail: TStringGrid;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    edtMaster_Descricao: TEdit;
    edtClient_ID: TEdit;
    edtClient_Nome: TEdit;
    edtMaster_Cadastro: TEdit;
    edtMaster_Alteracao: TEdit;
    Edit7: TEdit;
    Label8: TLabel;
    Button1: TButton;
    edtMaster_ID: TEdit;
    procedure Button3Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure StringGridMasterSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
    ARowAtual: Integer;

    oConn: IDBConnection;
    oMaster: TSessionManager<Tmaster>;
    oDetail: TSessionManager<Tdetail>;
    oClient: TSessionManager<Tclient>;
    oLookup: TSessionManager<Tlookup>;

    procedure MasterStringGridFill;
    procedure MasterStringGridDefinitions;
    procedure SetValuesEdits;
    procedure ClearValueEdits;
    procedure DetailStringGridFill;
    procedure DetailStringGridDefinitions;
  public
    { Public declarations }
  end;

var
  Form3: TForm3;

implementation

uses
  StrUtils;

{$R *.dfm}

procedure TForm3.Button1Click(Sender: TObject);
begin
  /// Guarda a linha atual.
  ARowAtual := StringGridMaster.Row;
  /// Busca o proximo pacote de registros
  oMaster.GetNextPacket;
  /// Atualiza o StringGrid
  MasterStringGridFill;
  /// Posicional a linha que estava antes de preencher o StringGrid.
  StringGridMaster.Row := ARowAtual;
end;

procedure TForm3.Button3Click(Sender: TObject);
begin
  oMaster.Open;
  ///
  MasterStringGridFill;
end;

procedure TForm3.Button4Click(Sender: TObject);
begin
  oMaster.Manager.Clear;
  /// Limpa o StringGrid;
  StringGridMaster.RowCount := 1;
  ///
  ClearValueEdits;
end;

procedure TForm3.ClearValueEdits;
begin
  edtMaster_ID.Clear;
  edtMaster_Descricao.Clear;
  edtClient_ID.Clear;
  edtClient_Nome.Clear;
  edtMaster_Cadastro.Clear;
  edtMaster_Alteracao.Clear;
end;

procedure TForm3.DetailStringGridDefinitions;
var
  iFor: Integer;
begin
  StringGridDetail.ColCount := 5;

  for iFor := 0 to StringGridDetail.ColCount -1 do
    StringGridDetail.ColWidths[iFor] := 150;

  StringGridDetail.Cols[0].Text := 'Detail_ID';
  StringGridDetail.Cols[1].Text := 'Master_ID';
  StringGridDetail.Cols[2].Text := 'Lookup_ID';
  StringGridDetail.Cols[3].Text := 'Lookup_Descricao';
  StringGridDetail.Cols[4].Text := 'Pre�o Unit�rio';
end;

procedure TForm3.DetailStringGridFill;
var
  LDetail: Tdetail;
begin
  StringGridDetail.RowCount := 1;
  for LDetail in oDetail.Manager.List do
  begin
    StringGridDetail.Cells[0, StringGridDetail.RowCount] := IntToStr(LDetail.detail_id);
    StringGridDetail.Cells[1, StringGridDetail.RowCount] := IntToStr(LDetail.master_id);
    StringGridDetail.Cells[2, StringGridDetail.RowCount] := IntToStr(LDetail.lookup_id);
    StringGridDetail.Cells[3, StringGridDetail.RowCount] := LDetail.lookup_description;
    StringGridDetail.Cells[4, StringGridDetail.RowCount] := FloatToStr(LDetail.price);
    TStringGridHack(StringGridDetail).InsertRow(1);
  end;
  if StringGridDetail.RowCount > 1 then
    StringGridDetail.FixedRows := 1;
  ///
  oDetail.Manager.Find(ARowAtual -1);
end;

procedure TForm3.FormCreate(Sender: TObject);
var
  CanSelect: Boolean;
begin
  ARowAtual := 1;
  MasterStringGridDefinitions;
  DetailStringGridDefinitions;

  /// <summary>
  /// Variaveis declaradas em { Private declarations } acima.
  /// </summary>
  // Inst�ncia da class de conex�o via FireDAC
  oConn := TFactoryFireDAC.Create(FDConnection1, dnSQLite);

  /// Class Adapter
  /// Par�metros: (IDBConnection, TClientDataSet)
  /// 10 representa a quantidadede registros por pacote de retorno para um select muito grande,
  /// defina o quanto achar melhor para sua necessiade
  oMaster := TSessionManager<Tmaster>.Create(oConn, 10);

  /// Relacionamento Master-Detail
  oDetail := TSessionManager<Tdetail>.Create(oConn);

  /// Relacionamento 1:1
  oClient := TSessionManager<Tclient>.Create(oConn);

  /// Lookup lista de registro (DBLookupComboBox)
  oLookup := TSessionManager<Tlookup>.Create(oConn);

  oMaster.Open;
  MasterStringGridFill;
  /// <summary>
  /// Dispara o evento onde abre as sub-tabelas
  /// </summary>
  ARowAtual := 0;
  StringGridMaster.OnSelectCell(StringGridMaster, 0, 1, CanSelect);
end;

procedure TForm3.FormDestroy(Sender: TObject);
begin
  oLookup.Free;
  oClient.Free;
  oDetail.Free;
  oMaster.Free;
  inherited;
end;

procedure TForm3.SetValuesEdits;
begin
  edtMaster_ID.Text := IntToStr(oMaster.Manager.Current.master_id);
  edtMaster_Descricao.Text := oMaster.Manager.Current.description;
  edtClient_ID.Text := IntToStr(oMaster.Manager.Current.client_id);
  edtClient_Nome.Text := oMaster.Manager.Current.client_name;
  edtMaster_Cadastro.Text := DateTimeToStr(oMaster.Manager.Current.registerdate);
  edtMaster_Alteracao.Text := DateTimeToStr(oMaster.Manager.Current.updatedate);
end;

procedure TForm3.StringGridMasterSelectCell(Sender: TObject; ACol, ARow: Integer;
  var CanSelect: Boolean);
begin
  if ARow > 0 then
  begin
    if ARowAtual <> ARow then
    begin
      ARowAtual := ARow;
      oMaster.Manager.Find(ARowAtual -1);
      SetValuesEdits;
      /// <summary>
      /// Abre sub-tabelas
      /// </summary>
      oDetail.Close;
      oDetail.Open(CreateCriteria.Select
                                 .All
                                 .From('detail')
                                 .Where('master_id = ' + IntToStr(oMaster.Manager.Current.master_id))
                                 .OrderBy('detail_id'));
      DetailStringGridFill;

      oClient.Close;
      oClient.Open(CreateCriteria.Select
                                 .All
                                 .From('client')
                                 .Where('client_id = ' + IntToStr(oMaster.Manager.Current.client_id)));
      if oClient.RecordCount > 0 then
        edtClient_Nome.Text := oClient.Manager.Current.client_name;
    end;
  end;
end;

procedure TForm3.MasterStringGridDefinitions;
var
  iFor: Integer;
begin
  StringGridMaster.ColCount := 6;

  for iFor := 0 to StringGridMaster.ColCount -1 do
    StringGridMaster.ColWidths[iFor] := 150;

  StringGridMaster.Cols[0].Text := 'ID';
  StringGridMaster.Cols[1].Text := 'Descri��o';
  StringGridMaster.Cols[2].Text := 'Data Cadastro';
  StringGridMaster.Cols[3].Text := 'Data Altera��o';
  StringGridMaster.Cols[4].Text := 'Cliente ID';
  StringGridMaster.Cols[5].Text := 'Cliente Nome';
end;

procedure TForm3.MasterStringGridFill;
var
  LMaster: Tmaster;
begin
  StringGridMaster.RowCount := 1;
  for LMaster in oMaster.Manager.List do
  begin
    StringGridMaster.Cells[0, StringGridMaster.RowCount] := IntToStr(LMaster.master_id);
    StringGridMaster.Cells[1, StringGridMaster.RowCount] := LMaster.description;
    StringGridMaster.Cells[2, StringGridMaster.RowCount] := DateTimeToStr(LMaster.registerdate);
    StringGridMaster.Cells[3, StringGridMaster.RowCount] := DateTimeToStr(LMaster.updatedate);
    StringGridMaster.Cells[4, StringGridMaster.RowCount] := IntToStr(LMaster.client_id);
    StringGridMaster.Cells[5, StringGridMaster.RowCount] := LMaster.client_name;
    TStringGridHack(StringGridMaster).InsertRow(1);
  end;
  if StringGridMaster.RowCount > 1 then
    StringGridMaster.FixedRows := 1;
  ///
  oMaster.Manager.Find(ARowAtual -1);
  SetValuesEdits;
end;

{ TStringGridHack }

procedure TStringGridHack.DeleteRow(ARow: Longint);
var
  GemRow: Integer;
begin
  GemRow := Row;
  if RowCount > FixedRows + 1 then
    inherited DeleteRow(ARow)
  else
    Rows[ARow].Clear;
  if GemRow < RowCount then
    Row := GemRow;
end;

procedure TStringGridHack.InsertRow(ARow: Longint);
var
  GemRow: Integer;
begin
  GemRow := Row;
  while ARow < FixedRows do
    Inc(ARow);
  RowCount := RowCount + 1;
  Row := GemRow;
end;

end.
