object Form3: TForm3
  Left = 0
  Top = 0
  Caption = 'Form3'
  ClientHeight = 453
  ClientWidth = 864
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Label7: TLabel
    Left = 12
    Top = 301
    Width = 33
    Height = 13
    Caption = 'Detail'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label1: TLabel
    Left = 12
    Top = 168
    Width = 47
    Height = 13
    Caption = 'Master ID'
  end
  object Label2: TLabel
    Left = 144
    Top = 168
    Width = 89
    Height = 13
    Caption = 'Master Description'
  end
  object Label3: TLabel
    Left = 12
    Top = 210
    Width = 41
    Height = 13
    Caption = 'Client ID'
  end
  object Label4: TLabel
    Left = 144
    Top = 210
    Width = 83
    Height = 13
    Caption = 'Client Description'
  end
  object Label5: TLabel
    Left = 12
    Top = 254
    Width = 76
    Height = 13
    Caption = 'Master Register'
  end
  object Label6: TLabel
    Left = 144
    Top = 254
    Width = 71
    Height = 13
    Caption = 'Master Update'
  end
  object Label8: TLabel
    Left = 668
    Top = 419
    Width = 61
    Height = 13
    Caption = 'Total Pre'#231'o :'
  end
  object Button2: TButton
    Left = 178
    Top = 419
    Width = 113
    Height = 25
    Caption = 'ApplyUpdates'
    TabOrder = 1
  end
  object Button3: TButton
    Left = 12
    Top = 418
    Width = 75
    Height = 25
    Caption = 'Open'
    TabOrder = 2
    OnClick = Button3Click
  end
  object Button4: TButton
    Left = 93
    Top = 419
    Width = 75
    Height = 25
    Caption = 'Close'
    TabOrder = 3
    OnClick = Button4Click
  end
  object StringGridMaster: TStringGrid
    Left = 12
    Top = 6
    Width = 844
    Height = 120
    ColCount = 6
    DefaultRowHeight = 20
    FixedCols = 0
    RowCount = 1
    FixedRows = 0
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goDrawFocusSelected]
    TabOrder = 0
    OnSelectCell = StringGridMasterSelectCell
    ColWidths = (
      64
      64
      64
      64
      64
      64)
    RowHeights = (
      20)
  end
  object StringGridDetail: TStringGrid
    Left = 12
    Top = 316
    Width = 844
    Height = 93
    DefaultRowHeight = 20
    FixedCols = 0
    RowCount = 1
    FixedRows = 0
    TabOrder = 4
    ColWidths = (
      64
      64
      64
      64
      64)
    RowHeights = (
      20)
  end
  object edtMaster_Descricao: TEdit
    Left = 144
    Top = 184
    Width = 712
    Height = 21
    TabOrder = 5
  end
  object edtClient_ID: TEdit
    Left = 12
    Top = 226
    Width = 121
    Height = 21
    TabOrder = 6
  end
  object edtClient_Nome: TEdit
    Left = 144
    Top = 226
    Width = 712
    Height = 21
    TabOrder = 7
  end
  object edtMaster_Cadastro: TEdit
    Left = 12
    Top = 270
    Width = 121
    Height = 21
    TabOrder = 8
  end
  object edtMaster_Alteracao: TEdit
    Left = 144
    Top = 270
    Width = 121
    Height = 21
    TabOrder = 9
  end
  object Edit7: TEdit
    Left = 735
    Top = 415
    Width = 121
    Height = 21
    TabOrder = 10
  end
  object Button1: TButton
    Left = 13
    Top = 132
    Width = 155
    Height = 25
    Caption = 'Proximo Pacote de Registros'
    TabOrder = 11
    OnClick = Button1Click
  end
  object edtMaster_ID: TEdit
    Left = 12
    Top = 184
    Width = 121
    Height = 21
    TabOrder = 12
  end
  object FDConnection1: TFDConnection
    Params.Strings = (
      'Database=..\Database\database.db3'
      'DriverID=SQLite')
    LoginPrompt = False
    Left = 154
    Top = 42
  end
  object FDPhysSQLiteDriverLink1: TFDPhysSQLiteDriverLink
    Left = 218
    Top = 42
  end
  object FDGUIxWaitCursor1: TFDGUIxWaitCursor
    Provider = 'Forms'
    Left = 86
    Top = 42
  end
end
